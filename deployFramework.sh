# SUBWAW

## Installation :

#Sur une machine avec docker (https://docs.docker.com/engine/install/ubuntu/)
# ./deployFramework.sh frameworkDir dataDir resultDir local/dockerhub login passwd

instDir=$1 # instDir=/scratch/
dataDir=$2 # dataDir=/biocodatas/MBBworkflows/khalid/Data
resultDir=$3 # resultDir=/biocodatas/MBBworkflows/khalid/Results
imgSource=$4 # imgSource=local ou imgSource=dockerhub
subwawUser=$5 
subwawUserPwd=$6

if [ -z "${subwawUser}" ]; then
 subwawUser="admin"
fi

if [ -z "{$subwawUserPwd}" ]; then
 subwawUserPwd="admin"
fi



mkdir $instDir/mbb-framework
cd $instDir/mbb-framework


 # cloner les trois dépôts ( waw, subwaw et sag)


git clone https://gitlab.mbb.univ-montp2.fr/jlopez/subwaw.git #must be public

# GoJS library licensing.
echo 'You need to get a copy of GoJS (JavaScript diagramming library) after accepting The GoJS software license terms at https://gojs.net/latest/license.html  !!!'
mkdir -p /inc/js/node_modules/
cd subwaw/inc/js/node_modules/
wget https://github.com/NorthwoodsSoftware/GoJS/archive/refs/tags/v2.1.2.tar.gz
tar -zxvf v2.1.2.tar.gz
mv GoJS-2.1.2 gojs


git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw.git 
git clone https://gitlab.mbb.univ-montp2.fr/jlopez/sag.git
cd subwaw


# create a config file (taper le code à partir de cat jusqu'à EOF)


cat << EOF > conf/Conf.php
<?php

class Conf {

    public static \$VERSION = 2;

    public static \$DB_NAME       = 'subwaw';
    public static \$DB_HOSTNAME   = 'localhost';
    public static \$DB_USERNAME   = 'root';
    public static \$DB_PP         = 'mbb';

 
    public static \$KK = "00a0f54bf0b225204124985243c5390baa9d8bf2";

    public static \$PATH_WAW = "/var/www/html/waw/";

    public static \$PATH_YAML = "/var/www/html/waw/tools/";
    
    public static \$PATH_YAML_WORKFLOWS = "/var/www/html/waw/workflows/";

    public static \$PATH_YAML_RAW_INPUTS = "/var/www/html/waw/raw_inputs/";

    public static \$PATH_YAML_OUTPUTS = "/var/www/html/waw/output/";

    public static \$PATH_SAG = "/var/www/html/sag/";

    public static \$ACTIVATE_RUN = true;

    public static function dbEncodePass(\$p) {
        return sha1(\$p);
    }

    public static function ldapEncodePass(\$p) {
        return "{SHA}".base64_encode(pack("H*", sha1(\$p)));
    }

}

EOF


 # à faire avec prudence 

chmod -R 777 $instDir/mbb-framework

if [ "${imgSource}" == "local" ]
then
 # construction de l'image subwaw avec les dépendances pour pouvoir lancer les workflows directement depuis l'interface:
 #A noter que pour le moment il manque certaines dépendances (il faudra les ajouter dans le Dockerfile) !!!
 sudo docker build -t subwaw-local .
else
 # utilisation de l'image depuis dockerhub prend 7 min !
 docker pull mbbteam/subwaw-local:latest
fi
#to push to docker-hub
#docker login --username=yourhubusername --email=youremail@company.com
# docker tag subwaw-local mbbteam/subwaw-local:latest
# docker push mbbteam/subwaw-local:latest

#Créer un dossier qui va contenir les données à partager avec les containers : 

mkdir -p $dataDir


#Créer un dossier qui va contenir les résultats des différents workflows :

mkdir -p $resultDir
chmod -R 777 $resultDir


# mapper les dossier locaux et ceux à l'intérieur du container


DOCK_VOL="--mount type=bind,src=$dataDir,dst=/Data \
--mount type=bind,src=$resultDir,dst=/Results \
--mount type=bind,src=$instDir/mbb-framework/sag,dst=/var/www/html/sag \
--mount type=bind,src=$instDir/mbb-framework/waw,dst=/var/www/html/waw \
--mount type=bind,src=$instDir/mbb-framework/subwaw,dst=/var/www/html/subwaw"


#Lancer un container :
if [ "$imgSource" == "local" ]
then
 cmd="sudo docker run --name sub-local -d -p 80:80 -p 90:3838 $DOCK_VOL subwaw-local" 
else
 cmd="sudo docker run --name sub-local -d -p 80:80 -p 90:3838 $DOCK_VOL mbbteam/subwaw-local"
fi

$cmd

# if port 80 is used by an other process
# sudo apt-get install net-tools
# netstat -ltnp | grep -w ':80' 

echo "to rerun the containtainer :"
echo $cmd > rerun.sh

# ajouter le controle d'accès à la page web subwaw via .htaccess
#il y a un .htaccess dans /var/www/html (voir Dockerfile) qui lit les mdp dans /var/www/html/subwaw

# ajouter l'utilisateur au htpasswd
if [ -n "{$subwawUserPwd}" ]; then
  sudo docker exec -i -t  sub-local htpasswd -cb /var/www/html/subwaw/.htpasswd ${subwawUser} ${subwawUserPwd}
else
  # ajouter l'utilisateur au htpasswd [methode interactive]
  sudo docker exec -i -t  sub-local htpasswd -c /var/www/html/subwaw/.htpasswd ${subwawUser}
  # decommentez pour utiliser pwgen
  #sudo docker exec -i -t  sub-local htpasswd -bc /var/www/html/subwaw/.htpasswd ${subwawUser} $(pwgen -cB 8 1)
fi
# à refaire pour ajouter de nouveaux utilisateurs ou changer de mdp pour un user

# restart apache2 server
sudo docker exec -i -t  sub-local /etc/init.d/apache2 reload


#L'interface subwaw est accessible à l'adresse 127.0.0.1/subwaw depuis la même machine.

#Sinon exécuter cette commande pour avoir l'adresse IP public de la machine sur laquelle vous venez de déployer mbb-framework :
 

wget -qO- https://ipinfo.io/ip | awk -v port=80 '{print "You can access the SubWaw workflow generator interface at :  http://"$1":"port}'



# pour avoir une console ssh à l'interieur du container :
# sudo docker exec -i -t  sub-local /bin/bash
