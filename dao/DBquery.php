<?php

require_once(__DIR__.'/../conf/Conf.php');
require_once(__DIR__.'/../model/Tool.php');
require_once(__DIR__.'/../model/Grade.php');
require_once(__DIR__.'/../model/Label.php');
require_once(__DIR__.'/../model/ToolLabel.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/Workflow.php');
require_once(__DIR__.'/../model/WorkflowLabel.php');

class DBqueryLite {

    var $dbh = NULL;

    public function __construct() {

        if(!class_exists('SQLite3'))
            die("SQLite 3 NOT supported.");

        $this->dbh = new SQLite3(__DIR__.'/../data/subwaw.db');

        if(!$this->dbh) {
            var_dump( $this->dbh->lastErrorMsg());
            die();
        }
    }

    public function escape($value) {
        return SQLite3::escapeString($value);
    }

    public function create($object) {

        $result = $this->dbh->query($object->getInsert());

        if($result) {
            
        } else {
            var_dump( $this->dbh->lastErrorMsg());
            die();
        }
    }



    public function update($object) {
        $result = $this->dbh->query($object->getUpdate()); 
        if($result) {
            
        } else {
            var_dump( $this->dbh->lastErrorMsg());
            die();
        }
    }


    public function delete($object) {
        $result = $this->dbh->query($object->getDelete());
        if($result) {
            
        } else {
            var_dump( $this->dbh->lastErrorMsg());
            die();
        }
    }

    public function verifyPass($username, $pass) {
        if($username == "admin" && Conf::dbEncodePass($pass)==Conf::$KK) {
            return 1;
        } else {
            # if you want guest admin
            //if($username == "guest" && $pass == "2019mbb") {
            //    return 2;
            //} else {
                return 0;
            //}
        }
    }

    public function getWorkflows() {
        $sql = "SELECT * FROM `Workflow`;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $workflow = new Workflow($row['ID'], $row['name'], $row['docker_name'], $row['description'], $row['version'], $row['author'], $row['creationDate'], $row['updateDate'], $row['tags'], $row['graphicYAML'], $row['yaml'], $row['git']);
            array_push($results, $workflow);
        }
        return $results;
    }

    public function getTools() {
        $sql = "SELECT * FROM `Tool`;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $tool = new Tool($row['ID'], $row['TID'], $row['name'], $row['description'], $row['version'], $row['website'], $row['git'], $row['documentation'], $row['article'], $row['multiQC'], $row['install'], $row['yaml'], $row['author'], $row['creationDate'], $row['updateDate'], $row['tags']);
            array_push($results, $tool);
        }
        return $results;
    }

    public function getToolWithId($id) {
        $sql = "SELECT * FROM `Tool` WHERE `ID` = '$id';";
        $result = $this->dbh->query($sql);

        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $tool = new Tool($row['ID'], $row['TID'], $row['name'], $row['description'], $row['version'], $row['website'], $row['git'], $row['documentation'], $row['article'], $row['multiQC'], $row['install'], $row['yaml'], $row['author'], $row['creationDate'], $row['updateDate'], $row['tags']);
            return $tool;
        }
        return NULL;
    }

    public function getWorkflowWithId($id) {
        $sql = "SELECT * FROM `Workflow` WHERE `ID` = '$id';";
        $result = $this->dbh->query($sql);

        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $workflow = new Workflow($row['ID'], $row['name'], $row['docker_name'], $row['description'], $row['version'], $row['author'], $row['creationDate'], $row['updateDate'], $row['tags'], $row['graphicYAML'], $row['yaml'], $row['git']);
            return $workflow;
        }
        return NULL;
    }

    public function getNumberTool() {
        $sql = 'SELECT count(*) as number FROM `Tool` WHERE 1';
        $result = $this->dbh->query($sql);
        if($result) {
            while($row = $result->fetchArray(SQLITE3_ASSOC)){
                return $row['number'];
            }
        }

        return 0;
    }

    public function getNumberWorkflow() {
        $sql = 'SELECT count(*) as number FROM `Workflow` WHERE 1';
        $result = $this->dbh->query($sql);
        if($result) {
            while($row = $result->fetchArray(SQLITE3_ASSOC)){
                return $row['number'];
            }
        }

        return 0;
    }

    public function getUsers() {
        $sql = "SELECT * FROM `User`;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $u = new User($row['ID'], $row['login'], $row['gradeId']);
            array_push($results, $u);
        }
        return $results;
    }

    public function getGradeWithLogin($login) {
        $sql = "SELECT * FROM `User` WHERE `login` = '$login';";
        $result = $this->dbh->query($sql);

        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $u = new User($row['ID'], $row['login'], $row['gradeId']);
            return $this->getGradeWithId($u->gradeId);
        }
  
        return $this->getLowerGrades();
    }

    public function getGradeWithId($ID) {
        $sql = "SELECT * FROM `Grade` WHERE `ID` = '$ID';";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $m = new Grade($row['ID'], $row['name'], $row['level']);
            return $m;
        }
        return $results;
    }

    public function getLowerGrades() {
        $sql = "SELECT * FROM `Grade` ORDER BY `level` DESC LIMIT 1;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $m = new Grade($row['ID'], $row['name'], $row['level']);
            return $m;
        }
        return $results;
    }

    public function getMaxGrades() {
        $sql = "SELECT * FROM `Grade` ORDER BY `level` ASC LIMIT 1;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $m = new Grade($row['ID'], $row['name'], $row['level']);
            return $m;
        }
        return $results;
    }

    public function getLabelsWithGrade($grade) {

        $labels = $this->getLabels();

        $results = array();

        foreach ($labels as $label){ 

            $grd = $this->getGradeWithId($label->gradeId);

            if($grd->level >= $grade->level) {
                array_push($results, $label);
            }

        }

        return $results;
    }

    public function getLabels() {
        $sql = "SELECT * FROM `Label`;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $l = new Label($row['ID'], $row['name'], $row['color'], $row['gradeId']);
            array_push($results, $l);
        }
        return $results;
    }

    public function getLabelsWithId($id) {
        $sql = "SELECT * FROM `Label` WHERE `ID` = $id;";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $l = new Label($row['ID'], $row['name'], $row['color'], $row['gradeId']);
            return $l;
        }
        return $results;
    }

    public function getLabelWithToolId($id) {
        $sql = "SELECT * FROM `ToolLabel` WHERE `toolId` = $id";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $l = $this->getLabelsWithId($row['labelId']);
            array_push($results, $l);
        }
        return $results;
    }

    public function getLabelWithWorkflowId($id) {
        $sql = "SELECT * FROM `WorkflowLabel` WHERE `workflowId` = $id";
        $result = $this->dbh->query($sql);
        $results = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $l = $this->getLabelsWithId($row['labelId']);
            array_push($results, $l);
        }
        return $results;
    }
}

class DBquery
{


    var $dbh = NULL;

    public function __construct() {
        $this->openConnection();
    }

    /**
     * For open a database connection
     **/
    public function openConnection() {
        $this->dbh = mysqli_connect(Conf::$DB_HOSTNAME, conf::$DB_USERNAME, Conf::$DB_PP, Conf::$DB_NAME);

        if(!$this->dbh){
            die('Could not connect: ' . mysqli_error($this->dbh));
        }

        if (mysqli_connect_errno()) {
            printf("Echec connection : %s\n", mysqli_connect_error());
            exit();
        }
    }

    /**
     * Escapes special characters in a string for use in an SQL statement
     * @param string $value
     * @return string
     */
    public function escape($value) {
        return mysqli_real_escape_string($this->dbh, $value);
    }


    public function create($object) {

        $result = mysqli_query($this->dbh, $object->getInsert());

        if($result) {
            
        } else {
            var_dump(mysqli_error($this->dbh));
            die();
        }
    }



    public function update($object) {
        $result = mysqli_query($this->dbh, $object->getUpdate()); 
        if($result) {
            
        } else {
            var_dump(mysqli_error($this->dbh));
            die();
        }
    }


    public function delete($object) {
        $result = mysqli_query($this->dbh, $object->getDelete());
        if($result) {
            
        } else {
            var_dump(mysqli_error($this->dbh));
            die();
        }
    }

    public function verifyPass($username, $pass) {
        if($username == "admin" && Conf::dbEncodePass($pass)==Conf::$KK) {
            return 1;
        } else {
            # if you want guest admin
            //if($username == "guest" && $pass == "2019mbb") {
            //    return 2;
            //} else {
                return 0;
            //}
        }
    }

    public function getTools() {
        $sql = "SELECT * FROM `Tool`;";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $tool = new Tool($row['ID'], $row['TID'], $row['name'], $row['description'], $row['version'], $row['website'], $row['git'], $row['documentation'], $row['article'], $row['multiQC'], $row['install'], $row['yaml'], $row['author'], $row['creationDate'], $row['updateDate'], $row['tags']);
            array_push($results, $tool);
        }
        return $results;
    }

    public function getToolWithId($id) {
        $sql = "SELECT * FROM `Tool` WHERE `ID` = '$id';";
        $result = mysqli_query($this->dbh, $sql);

        while($row = mysqli_fetch_assoc($result)){
            $tool = new Tool($row['ID'], $row['TID'], $row['name'], $row['description'], $row['version'], $row['website'], $row['git'], $row['documentation'], $row['article'], $row['multiQC'], $row['install'], $row['yaml'], $row['author'], $row['creationDate'], $row['updateDate'], $row['tags']);
            return $tool;
        }
        return NULL;
    }

    public function getNumberTool() {
        $sql = 'SELECT count(*) as number FROM `Tool` WHERE 1';
        $result = mysqli_query($this->dbh, $sql);
        if($result) {

            while($row = mysqli_fetch_assoc($result)){
                return $row['number'];
            }

        }

        return 0;
    }

    public function getUsers() {
        $sql = "SELECT * FROM `User`;";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $u = new User($row['ID'], $row['login'], $row['gradeId']);
            array_push($results, $u);
        }
        return $results;
    }

    public function getGradeWithLogin($login) {
        $sql = "SELECT * FROM `User` WHERE `login` = '$login';";
        $result = mysqli_query($this->dbh, $sql);

        while($row = mysqli_fetch_assoc($result)){
            $u = new User($row['ID'], $row['login'], $row['gradeId']);
            return $this->getGradeWithId($u->gradeId);
        }
  
        return $this->getLowerGrades();
    }

    public function getGradeWithId($ID) {
        $sql = "SELECT * FROM `Grade` WHERE `ID` = '$ID';";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $m = new Grade($row['ID'], $row['name'], $row['level']);
            return $m;
        }
        return $results;
    }

    public function getLowerGrades() {
        $sql = "SELECT * FROM `Grade` ORDER BY `level` DESC LIMIT 1;";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $m = new Grade($row['ID'], $row['name'], $row['level']);
            return $m;
        }
        return $results;
    }

    public function getMaxGrades() {
        $sql = "SELECT * FROM `Grade` ORDER BY `level` ASC LIMIT 1;";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $m = new Grade($row['ID'], $row['name'], $row['level']);
            return $m;
        }
        return $results;
    }

    public function getLabelsWithGrade($grade) {

        $labels = $this->getLabels();

        $results = array();

        foreach ($labels as $label){ 

            $grd = $this->getGradeWithId($label->gradeId);

            if($grd->level >= $grade->level) {
                array_push($results, $label);
            }

        }

        return $results;
    }

    public function getLabels() {
        $sql = "SELECT * FROM `Label`;";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $l = new Label($row['ID'], $row['name'], $row['color'], $row['gradeId']);
            array_push($results, $l);
        }
        return $results;
    }

    public function getLabelsWithId($id) {
        $sql = "SELECT * FROM `Label` WHERE `ID` = $id;";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $l = new Label($row['ID'], $row['name'], $row['color'], $row['gradeId']);
            return $l;
        }
        return $results;
    }

    public function getLabelWithToolId($id) {
        $sql = "SELECT * FROM `ToolLabel` WHERE `toolId` = $id";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $l = $this->getLabelsWithId($row['labelId']);
            array_push($results, $l);
        }
        return $results;
    }

    public function getLabelWithWorkflowId($id) {
        $sql = "SELECT * FROM `WorkflowLabel` WHERE `workflowId` = $id";
        $result = mysqli_query($this->dbh, $sql);
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            $l = $this->getLabelsWithId($row['labelId']);
            array_push($results, $l);
        }
        return $results;
    }


}

