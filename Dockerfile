FROM mbbteam/mbb_workflows_base:latest as alltools
RUN apt-get update


# # Copy this repo into place.
# VOLUME ["/var/www", "/etc/apache2/sites-enabled"]

# # Update the default apache site with the config we created.
# ADD apache-config.conf /etc/apache2/sites-enabled/000-default.conf

# RUN echo "ServerName 12.0.1" >> /etc/apache2/sites-enabled/000-default.conf

WORKDIR /var/www/html


#RUN chown -R www-data:www-data  /var/www/html
#RUN chmod -R +w  /var/www/html/waw/output


ENV PATH $PATH:/opt/biotools/ncbi-blast-2.12.0+/bin
RUN cd /opt/biotools/ \
 && wget -O ncbi-blast-2.12.0+.tar.gz https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.12.0/ncbi-blast-2.12.0+-x64-linux.tar.gz \
 && tar -xvzf ncbi-blast-2.12.0+.tar.gz




ENV AUGUSTUS_CONFIG_PATH /usr/share/augustus/config

RUN apt-get -y update \
 && apt-get -y install  augustus \
 && cd /opt/biotools/bin \
 && wget -O prodigal https://github.com/hyattpd/Prodigal/releases/download/v2.6.3/prodigal.linux \
 && chmod +x prodigal

#To allow www-data to write in augustus species 
#RUN chmod -R 777 /usr/share/augustus

RUN cd /opt/biotools \
 && wget http://eddylab.org/software/hmmer/hmmer-3.2.1.tar.gz \
 && tar -xvzf hmmer-3.2.1.tar.gz \
 && cd hmmer-3.2.1 \
 && ./configure --prefix /opt/biotools/ \
 && make -j 10 \
 && make install \
 && cd easel; make install \
 && cd /opt/biotools \
 && rm -r hmmer-3.2.1.tar.gz hmmer-3.2.1

RUN cd /opt/biotools \
 && wget -O sepp-4.3.10.tar.gz https://github.com/smirarab/sepp/archive/4.3.10.tar.gz \
 && tar -xvzf sepp-4.3.10.tar.gz \
 && cd sepp-4.3.10 \
 && python3 setup.py config -c \
 && python3 setup.py install \
 && cd .. && rm -r sepp-4.3.10 sepp-4.3.10.tar.gz

ENV BUSCO_CONFIG_FILE /opt/biotools/busco_config.ini
RUN cd /opt/biotools \
 && wget https://gitlab.com/ezlab/busco/-/archive/4.1.3/busco-4.1.3.tar.gz \
 && tar -xvzf busco-4.1.3.tar.gz \
 && cd busco-4.1.3 \
 && python3 setup.py install \
 && cd .. \
 && echo '[tblastn]\npath = /opt/biotools/ncbi-blast-2.12.0+/bin/\ncommand = tblastn\n' > busco_config.ini \
 && echo '[makeblastdb]\npath = /opt/biotools/ncbi-blast-2.12.0+/bin/\ncommand = makeblastdb\n' >> busco_config.ini \
 && echo '[augustus]\npath = /usr/bin/\ncommand = augustus\n' >> busco_config.ini \
 && echo '[etraining]\npath = /usr/bin/\ncommand = etraining\n' >> busco_config.ini \
 && echo '[gff2gbSmallDNA.pl]\npath = /usr/share/augustus/scripts/\ncommand = gff2gbSmallDNA.pl\n' >> busco_config.ini \
 && echo '[new_species.pl]\npath = /usr/share/augustus/scripts/\ncommand = new_species.pl\n' >> busco_config.ini \
 && echo '[optimize_augustus.pl]\npath = /usr/share/augustus/scripts/\ncommand = optimize_augustus.pl\n' >> busco_config.ini \
 && echo '[hmmsearch]\npath = /opt/biotools/bin/\ncommand = hmmsearch\n' >> busco_config.ini \
 && echo '[sepp]\npath = /usr/local/bin/\ncommand = run_sepp.py\n' >> busco_config.ini \
 && echo '[prodigal]\npath = /opt/biotools/bin/\ncommand = prodigal\n\n' >> busco_config.ini \
 && echo '[busco_run]' >> busco_config.ini


RUN apt-get update \
 && apt-get install -y libboost-dev libopenmpi-dev libsparsehash-dev bsdmainutils \
 && cd /opt/biotools \
 && wget https://github.com/bcgsc/abyss/archive/2.2.5.tar.gz \
 && tar -xvzf 2.2.5.tar.gz \
 && rm 2.2.5.tar.gz \
 && cd abyss-2.2.5 \
 && ./autogen.sh \
 && mkdir build && cd build \
 && ../configure \
 && make install-strip

RUN apt -y update && apt install -y tabix fastqc=0.11.9+dfsg-2

#resolve certificate problem

RUN trust_cert_file_location=`curl-config --ca` \
 && bash -c "echo -n | openssl s_client -showcerts -connect gitlab.mbb.univ-montp2.fr:443 -servername gitlab.mbb.univ-montp2.fr \
    2>/dev/null  | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'  \
    >> $trust_cert_file_location"

RUN cd /opt/biotools \
 && git clone https://gitlab.mbb.univ-montp2.fr/khalid/vcfparser.git

RUN Rscript -e 'devtools::install_github("thomasp85/patchwork")'

RUN Rscript -e 'install.packages(c("tidyverse","ggplot2"),Ncpus=8, clean=TRUE);'


RUN wget -O bowtie2-2.4.1-linux-x86_64.zip https://github.com/BenLangmead/bowtie2/releases/download/v2.4.1/bowtie2-2.4.1-linux-x86_64.zip \
 && unzip bowtie2-2.4.1-linux-x86_64.zip \
 && cp bowtie2-2.4.1-linux-x86_64/bowtie2* /usr/bin \
 && rm -rf bowtie2-2.4.1*

RUN apt-get install -y pigz

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

RUN cd /opt/biotools \
 && wget https://github.com/samtools/bcftools/releases/download/1.9/bcftools-1.9.tar.bz2 \
 && tar -xvjf bcftools-1.9.tar.bz2 \
 && cd bcftools-1.9 \
 && ./configure --prefix=/opt/biotools \
 && make -j 10 \
 && make install \
 && mv bcftools /opt/biotools/bin/ \
 && cd .. && rm -r bcftools-1.9.tar.bz2 bcftools-1.9

#Pour pouvoir lancer l'appli. shiny en root
RUN apt-get update && apt-get install sudo \
&& usermod -aG sudo www-data \
&& adduser www-data sudo \
&& echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers


RUN rm -rf /opt/biotools/*.gz \
    && cd /opt/biotools \
    && git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git \
    &&  cd mbb_mqc_plugin \
    &&  python3 setup.py install

RUN   cd /opt/biotools/ \
      && mkdir raxml_ng && cd raxml_ng \
      && wget https://github.com/amkozlov/raxml-ng/releases/download/1.0.1/raxml-ng_v1.0.1_linux_x86_64.zip \
      && unzip raxml-ng_v1.0.1_linux_x86_64.zip \
      && mv raxml-ng /opt/biotools/bin/ \
      && cd .. && rm -r raxml_ng

RUN apt-get update && apt-get install -y python3-numpy python3-pyqt5 python3-pyqt5.qtopengl python3-lxml python3-six xvfb
RUN pip3 install ete3==3.1.2

RUN cd /opt/biotools/ \
 && wget https://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_i86linux64.tar.gz \
 && tar -xvzf muscle3.8.31_i86linux64.tar.gz \
 && mv muscle3.8.31_i86linux64 bin/muscle \
 && rm muscle3.8.31_i86linux64.tar.gz

RUN cd /opt/biotools/ \
 && wget http://molevol.cmima.csic.es/castresana/Gblocks/Gblocks_Linux64_0.91b.tar.Z \
 && tar -xvzf Gblocks_Linux64_0.91b.tar.Z \
 && mv Gblocks_0.91b/Gblocks bin/ \
 && rm -r Gblocks_Linux64_0.91b.tar.Z Gblocks_0.91b

RUN apt-get update \
 && apt-get install -y openjdk-8-jdk \
 && apt-get install  -y ant

ENV PATH=${PATH}:/opt/biotools/jmodeltest-2.1.10
ENV JMODELTEST_HOME /opt/biotools/jmodeltest-2.1.10
RUN cd /opt/biotools \
 && wget https://github.com/ddarriba/jmodeltest2/files/157117/jmodeltest-2.1.10.tar.gz \
 && tar -xvzf jmodeltest-2.1.10.tar.gz \
 && rm jmodeltest-2.1.10.tar.gz \
 && cd $JMODELTEST_HOME 

RUN cd /opt/biotools/ \
 && wget https://mafft.cbrc.jp/alignment/software/mafft_7.471-1_amd64.deb \
 && dpkg -i mafft_7.471-1_amd64.deb \
 && rm mafft_7.471-1_amd64.deb

RUN cd /opt/biotools/ \
 && wget -O trimal_v1.4.1.tar.gz https://github.com/scapella/trimal/archive/v1.4.1.tar.gz \
 && tar -xvzf trimal_v1.4.1.tar.gz \
 && cd trimal-1.4.1/source \
 && make -j 8 \
 && mv statal trimal readal -t /opt/biotools/bin \
 && cd /opt/biotools \
 && rm -r trimal-1.4.1 trimal_v1.4.1.tar.gz

RUN cd /opt/biotools \
    && wget https://github.com/Cibiv/IQ-TREE/releases/download/v1.6.12/iqtree-1.6.12-Linux.tar.gz \
    && tar -xvzf iqtree-1.6.12-Linux.tar.gz \
    && mv iqtree-1.6.12-Linux/bin/iqtree bin \
    && rm -r iqtree-1.6.12-Linux iqtree-1.6.12-Linux.tar.gz

#environment variable must be explicitly set in /etc/environment which will be used by Rscript to set the BASH_ENV variable
RUN echo "export PATH=/opt/biotools/bin:$PATH" >> /etc/environment \
&& echo AUGUSTUS_CONFIG_PATH=/usr/share/augustus/config >> /etc/environment \
&& echo JMODELTEST_HOME=/opt/biotools/jmodeltest-2.1.10 >> /etc/environment \
&& echo BUSCO_CONFIG_FILE=/opt/biotools/busco_config.ini >> /etc/environment


RUN wget -O bowtie-1.3.0-linux-x86_64.zip https://sourceforge.net/projects/bowtie-bio/files/bowtie/1.3.0/bowtie-1.3.0-linux-x86_64.zip/download \
 && unzip bowtie-1.3.0-linux-x86_64.zip \
 && cp bowtie-1.3.0-linux-x86_64/bowtie* /usr/bin \
 && rm -rf bowtie-1.3.0*
          
RUN cd /opt/biotools/bin \
&& wget https://github.com/ekg/freebayes/releases/download/v1.3.1/freebayes-v1.3.1 -O freebayes \
&& chmod +x freebayes

#change for bcftools stats ploting
RUN sed -i 's/python/python3/g' /opt/biotools/bin/plot-vcfstats \
 && sed -i 's|11/2.54|20/2.54|' /opt/biotools/bin/plot-vcfstats \ 
 && sed -i 's|10/2.54|14/2.54|' /opt/biotools/bin/plot-vcfstats \
 && sed -i 's|window_len/2|window_len//2|g' /opt/biotools/bin/plot-vcfstats

RUN apt -y update && apt install -y tabix python3-matplotlib imagemagick

RUN apt -y update && apt install -y python-backports.functools-lru-cache

RUN cd /opt/biotools/bin \
&& wget https://github.com/ekg/freebayes/releases/download/v1.3.1/freebayes-v1.3.1 -O freebayes \
&& chmod +x freebayes


RUN cd /opt/biotools/bin && wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/biotools/conda


#RUN /opt/biotools/conda/bin/conda install -c conda-forge mamba 
#RUN /opt/biotools/conda/bin/mamba create -y -c conda-forge -c bioconda  -n deepvariant deepvariant==1.1.0 octopus

RUN /opt/biotools/conda/bin/conda create -y -c conda-forge -c bioconda  -n deepvariant deepvariant==1.1.0 
# howto activate this environment
# eval "$(conda shell.bash hook)"
# conda activate deepvariant

RUN /opt/biotools/conda/bin/conda install -y -c conda-forge -c bioconda octopus \
&& echo "export PATH=$PATH:/opt/biotools/conda/bin" >> /etc/environment
ENV PATH $PATH:/opt/biotools/conda/bin



RUN git clone https://github.com/vcftools/vcftools.git \
     && cd vcftools \
     &&  ./autogen.sh \
	 && ./configure \
     && make -j 8\
     && make install \
	 && cd .. && rm -rf vcftools*

RUN cd /opt/biotools \
&& wget https://github.com/bwa-mem2/bwa-mem2/releases/download/v2.0/bwa-mem2-2.0_x64-linux.tar.bz2 \
&& tar -xvjf bwa-mem2-2.0_x64-linux.tar.bz2 \
&& mv bwa-mem2-2.0_x64-linux/bwa-mem2* /opt/biotools/bin/ \
&& rm -r bwa-mem2-2.0_x64-linux bwa-mem2-2.0_x64-linux.tar.bz2 

RUN cd /opt/biotools \
 && wget -O GenomeAnalysisTK-3.6-0.tar.bz2 'https://storage.googleapis.com/gatk-software/package-archive/gatk/GenomeAnalysisTK-3.6-0-g89b7209.tar.bz2' \
 && mkdir gatk3 \
 && tar -C gatk3 -xjf GenomeAnalysisTK-3.6-0.tar.bz2 \
 && rm GenomeAnalysisTK-3.6-0.tar.bz2 \
 && rm -r gatk3/resources

RUN cd /opt/biotools/bin \
&& wget https://github.com/broadinstitute/picard/releases/download/2.20.8/picard.jar \
&& cd /opt/biotools \
&& git clone https://github.com/BELKHIR/vcfmultisampleparser.git

#to get the accompaning scripts (freebayes-parallel ...)
RUN cd /opt/biotools/ && git clone https://github.com/freebayes/freebayes.git 
RUN /opt/biotools/conda/bin/conda install -y -c bioconda vcflib parallel

RUN cd /opt/biotools \
&& wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip \
&& unzip Trimmomatic-0.38.zip \
&& echo -e '#!/bin/bash \n java -jar /opt/biotools/Trimmomatic-0.38/trimmomatic-0.38.jar' > bin/trimmomatic \
&& chmod 777 bin/trimmomatic \
&& rm Trimmomatic-0.38.zip

RUN cd /opt/biotools \
&& wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2 \
&& tar -xvjf bwa-0.7.17.tar.bz2 \
&& cd bwa-0.7.17 \
&& make -j 10 \
&& mv bwa ../bin/ \
&& cd .. \
&& rm -r bwa-0.7.17 bwa-0.7.17.tar.bz2

RUN cd /opt/biotools \
 && wget https://github.com/COMBINE-lab/salmon/releases/download/v1.3.0/salmon-1.3.0_linux_x86_64.tar.gz \
 && tar -zxvf salmon-1.3.0_linux_x86_64.tar.gz \
 && cd bin && ln -s ../salmon-latest_linux_x86_64/bin/salmon salmon \
 && cd .. && rm -r salmon-1.3.0_linux_x86_64.tar.gz salmon-latest_linux_x86_64/sample_data.tgz

RUN cd /opt/biotools \
    && wget http://cab.spbu.ru/files/release3.15.2/SPAdes-3.15.2-Linux.tar.gz\
    && tar -xvzf SPAdes-3.15.2-Linux.tar.gz \
    && rm SPAdes-3.15.2-Linux.tar.gz \
    && echo "export PATH=$PATH:/opt/biotools/SPAdes-3.15.2-Linux/bin" >> /etc/environment
ENV PATH $PATH:/opt/biotools/biotools/SPAdes-3.15.2-Linux/bin


RUN cd /opt/biotools \
    && git clone https://github.com/ablab/quast.git\
    && cd quast\
    && python3 setup.py install_full

RUN apt-get update\
    && apt-get install -y gcc zlib1g-dev pkg-config libfreetype6-dev libpng-dev
RUN cd /opt/biotools \
 && git clone https://gitlab.mbb.univ-montp2.fr/khalid/radsex \
 && cd radsex\
 && make -j 10 \
 && Rscript -e "devtools::install_github('SexGenomicsToolkit/sgtr');library('sgtr')" \
 && echo "export PATH=$PATH:/opt/biotools/radsex/bin" >> /etc/environment

ENV PATH $PATH:/opt/biotools/biotools/radsex/bin


RUN apt-get update\
    && apt-get install -y gnuplot \
    && cd /opt/biotools \
    && wget https://github.com/Illumina/interop/releases/download/v1.1.8/InterOp-1.1.8-Linux-GNU.tar.gz \
    && tar -xvzf InterOp-1.1.8-Linux-GNU.tar.gz \
    && rm InterOp-1.1.8-Linux-GNU.tar.gz \
    && echo "export PATH=$PATH:/opt/biotools/InterOp-1.1.8-Linux-GNU/bin" >> /etc/environment

ENV PATH /opt/biotools/InterOp-1.1.8-Linux-GNU/bin/:$PATH 

RUN Rscript -e 'BiocManager::install(c("SeqArray","SNPRelate", "ggtree","DESeq2","apeglm","BiocParallel","tximport","GenomicFeatures"), update = TRUE, ask = FALSE)'
RUN apt-get install -y libsodium-dev
RUN Rscript -e 'install.packages(c("latticeExtra","ape","pheatmap","gprofiler2", "shinyauthr","UpSetR") ,Ncpus=8, clean=TRUE);'



RUN  wget http://opengene.org/fastp/fastp.0.23.1 \
   && mv fastp.0.23.1 /opt/biotools/bin/fastp \
   && chmod a+x /opt/biotools/bin/fastp

RUN cd /tmp \
    && wget -O hisat2-2.2.1-linux-x86_64.zip https://cloud.biohpc.swmed.edu/index.php/s/oTtGWbWjaxsQ2Ho/download \
    && unzip hisat2-2.2.1-linux-x86_64.zip \
    && mv  hisat2-2.2.1 /opt/biotools/ \
    && rm -rf hisat2-2.2.1* \
    && echo export PATH=$PATH:/opt/biotools/hisat2-2.2.1 >> /etc/environment
ENV PATH /opt/biotools/hisat2-2.2.1:$PATH 

RUN pip3 install 'HTSeq==0.13.5' spython \
&& apt-get install -y ca-certificates


RUN apt-get install -y bioperl  circos \
&&  cd /opt/biotools && wget http://circos.ca/distribution/circos-current.tgz \
&& tar -xzf circos-current.tgz \
&& rm -rf circos-current.tgz \
&& mv circos* circos_current \
&& sed -i 's/max_points_per_track.*/max_points_per_track = 40000/' /opt/biotools/circos_current/etc/housekeeping.conf

ENV PATH /opt/biotools/circos_current/bin:$PATH \


RUN cd /opt/biotools \
&& wget -c https://github.com/linzhi2013/MitoZ/raw/master/version_2.4-alpha/release_MitoZ_v2.4-alpha.tar.bz2 \
&& tar -jxvf release_MitoZ_v2.4-alpha.tar.bz2 \
&& rm release_MitoZ_v2.4-alpha.tar.bz2 \
&& sed -i 's/choices=\[\"Chordata\", \"Arthropoda\"\]/choices=\[\"Chordata\", \"Arthropoda\", \"Echinodermata\", \"Annelida-segmented-worms\", \"Bryozoa\", \"Mollusca\", \"Nematoda\", \"Nemertea-ribbon-worms\", \"Porifera-sponges\"\]/' /opt/biotools/release_MitoZ_v2.4-alpha/MitoZ.py \
&& sed -i 's/^        yield rec/        try:\n            yield rec/' /opt/biotools/release_MitoZ_v2.4-alpha/bin/annotate/gene_feature.py \
&& sed -i 's/^        raise StopIteration/        except:\n            raise StopIteration/' /opt/biotools/release_MitoZ_v2.4-alpha/bin/annotate/gene_feature.py 
# see https://docs.python.org/3/whatsnew/3.7.html#changes-in-python-behavior



ENV PATH /opt/biotools/release_MitoZ_v2.4-alpha:$PATH 
RUN echo 'export PATH=$PATH:/opt/biotools/release_MitoZ_v2.4-alpha' >> /etc/environment \
&& python3 -c "from ete3 import NCBITaxa;ncbi = NCBITaxa();ncbi.update_taxonomy_database()"

RUN apt-get install -y ncbi-tools-bin infernal \
&&  cd /opt/biotools \
&& wget -c https://data.broadinstitute.org/igv/projects/downloads/2.6/IGV_Linux_2.6.2.zip \
&& unzip IGV_Linux_2.6.2.zip \
&& rm IGV_Linux_2.6.2.zip

ENV PATH /opt/biotools/IGV_Linux_2.6.2:$PATH 
RUN echo 'export PATH=$PATH:/opt/biotools/IGV_Linux_2.6.2' >> /etc/environment \
&& sed -i 's/-Xmx4g/-Xmx16g/' /opt/biotools/IGV_Linux_2.6.2/igv.sh

RUN cd /opt/biotools \
&& wget https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-2.17_x64-linux.tar.bz2 \
&& tar -xvjf minimap2-2.17_x64-linux.tar.bz2 \
&& rm minimap2-2.17_x64-linux.tar.bz2 \
&& echo 'export PATH=$PATH:/opt/biotools/minimap2-2.17_x64-linux' >> /etc/environment 

ENV PATH /opt/biotools/minimap2-2.17_x64-linux:$PATH 

RUN cd /opt/biotools \
&& git clone https://github.com/voutcn/megahit.git \
&& cd megahit \
&& git submodule update --init \
&& mkdir build && cd build \
&& cmake .. -DCMAKE_BUILD_TYPE=Release \
&& make -j8 \
&& make simple_test \
&& make install \
&& cd /opt/biotools \
&& rm -rf megahit

RUN pip install seaborn==0.10.1 &&   pip install nanoplot 
RUN cd /opt/biotools \
&& git clone https://github.com/fenderglass/Flye \
&& cd Flye \
&& python setup.py install
#&& make -j 8 \
#&& echo 'export PATH=$PATH:/opt/biotools/Flye/bin' >> /etc/environment

RUN cd /opt/biotools \
&& git clone https://github.com/raja-appuswamy/accel-align-release.git \
&& echo export 'PATH=/opt/biotools/accel-align-release:$PATH' >> /etc/environment \
&& apt-get install -y libtbb-dev \
&& cd /opt/biotools \
&& git clone https://github.com/amplab/snap.git \
&& cd snap \
&& make -j 8 \
&& echo export 'PATH=/opt/biotools/snap:$PATH' >> /etc/environment

# force redirect from http://127.0.0.1/ to http://127.0.0.1/subwaw 
RUN echo '<meta http-equiv="refresh" content="0; url=/subwaw/" />' > /var/www/html/index.html

# add htaccess control passwords must be generated in the host subwaw directory
# ex. using a running container 
# sudo docker exec -i -t  sub-local htpasswd -c /var/www/html/subwaw/.htpasswd admin
RUN echo '<Directory /var/www/html/>' >> /etc/apache2/apache2.conf \
&& echo '    Options Indexes FollowSymLinks' >> /etc/apache2/apache2.conf \
&& echo '    AllowOverride All' >> /etc/apache2/apache2.conf \
&& echo '    Require all granted' >> /etc/apache2/apache2.conf \
&& echo '</Directory>'  >> /etc/apache2/apache2.conf \
&& echo  'AuthType Basic'  > /var/www/html/.htaccess \
&& echo  'AuthType Basic' >> /var/www/html/.htaccess \
&& echo  'AuthName "Restricted Content"' >> /var/www/html/.htaccess \
&& echo  'AuthUserFile /var/www/html/subwaw/.htpasswd' >> /var/www/html/.htaccess \
&& echo  'Require valid-user' >> /var/www/html/.htaccess 


# Expose apache.
EXPOSE 80

# By default start up apache in the foreground, override with /bin/bash for interative.
CMD /usr/sbin/apache2ctl -D FOREGROUND

