# SUBWAW

## Installation :

> N.B : you have to Get a copy of GoJS (JavaScript diagramming library) after accepting The GoJS software license terms at https://gojs.net/latest/license.html  !!!


Sur une machine avec docker (https://docs.docker.com/engine/install/ubuntu/)

Deux possibilités de déploiement :

* Automatique via le script deployFramework.sh

* Pas à pas comme décrit ci-dessous :


```bash
instDir=$HOME

mkdir $instDir/mbb-framework
cd $instDir/mbb-framework
```

 # cloner les trois dépôts ( waw, subwaw et sag)

```bash
git clone https://gitlab.mbb.univ-montp2.fr/jlopez/subwaw.git
mkdir -p /inc/js/node_modules/
cd subwaw/inc/js/node_modules/
wget https://github.com/NorthwoodsSoftware/GoJS/archive/refs/tags/v2.1.2.tar.gz
tar -zxvf v2.1.2.tar.gz
mv GoJS-2.1.2 gojs

git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw.git 
git clone https://gitlab.mbb.univ-montp2.fr/jlopez/sag.git
cd subwaw
```

# create a config file (taper le code à partir de cat jusqu'à EOF)

```console
cat << EOF > conf/Conf.php
<?php

class Conf {

    public static \$VERSION = 2;

    public static \$DB_NAME       = 'subwaw';
    public static \$DB_HOSTNAME   = 'localhost';
    public static \$DB_USERNAME   = 'root';
    public static \$DB_PP         = 'mbb';

 
    public static \$KK = "00a0f54bf0b225204124985243c5390baa9d8bf2";

    public static \$PATH_WAW = "/var/www/html/waw/";

    public static \$PATH_YAML = "/var/www/html/waw/tools/";
    
    public static \$PATH_YAML_WORKFLOWS = "/var/www/html/waw/workflows/";

    public static \$PATH_YAML_RAW_INPUTS = "/var/www/html/waw/raw_inputs/";

    public static \$PATH_YAML_OUTPUTS = "/var/www/html/waw/output/";

    public static \$PATH_SAG = "/var/www/html/sag/";

    public static \$ACTIVATE_RUN = true;

    public static function dbEncodePass(\$p) {
        return sha1(\$p);
    }

    public static function ldapEncodePass(\$p) {
        return "{SHA}".base64_encode(pack("H*", sha1(\$p)));
    }

}

EOF
```

 # à faire avec prudence 

```bash
chmod -R 777 $instDir/mbb-framework
```

# construction de l'image subwaw avec les dépendances pour pouvoir lancer les workflows directement depuis l'interface:

A noter que pour le moment il manque certaines dépendances (il faudra les ajouter dans le Dockerfile) !!!

```bash
imgSource=dockerhub # utilisation de l'image depuis dockerhub prend 7 min !
#imgSource=local # # construction de l'image subwaw avec les dépendances pour pouvoir lancer les workflows directement depuis l'interface
if [ "$imgSource" == "local" ]
then
 #A noter que pour le moment il manque certaines dépendances (il faudra les ajouter dans le Dockerfile) !!!
 sudo docker build -t subwaw-local .
else
 docker pull mbbteam/subwaw-local:latest
fi

```

Créer un dossier qui va contenir les données à partager avec les containers : 

```bash
mkdir $instDir/Data
```

Créer un dossier qui va contenir les résultats des différents workflows :

```bash
mkdir $instDir/Results
chmod -R 777 $instDir/Results
```

# mapper les dossier locaux et ceux à l'intérieur du container

```bash
DOCK_VOL="--mount type=bind,src=$dataDir,dst=/Data \
--mount type=bind,src=$resultDir,dst=/Results \
--mount type=bind,src=$instDir/mbb-framework/sag,dst=/var/www/html/sag \
--mount type=bind,src=$instDir/mbb-framework/waw,dst=/var/www/html/waw \
--mount type=bind,src=$instDir/mbb-framework/subwaw,dst=/var/www/html/subwaw"
```

Lancer un container :
#ici on suppose que les ports tcp 80 et 90 sont libres

```bash
#Lancer un container :
if [ "$imgSource" == "local" ]
then
 cmd="sudo docker run --name sub-local -d -p 80:80 -p 90:3838 $DOCK_VOL subwaw-local" 
else
 cmd="sudo docker run --name sub-local -d -p 80:80 -p 90:3838 $DOCK_VOL mbbteam/subwaw-local"
fi

$cmd
```
# ajouter le controle d'accès à la page web subwaw via .htaccess
#il y a un .htaccess dans /var/www/html (voir Dockerfile) qui lit les mdp dans /var/www/html/subwaw

# ajouter l'utilisateur au htpasswd
subwawUser=admin
subwawUserPwd=MOTdePAss à chosir
sudo docker exec -i -t  sub-local htpasswd -cb /var/www/html/subwaw/.htpasswd $subwawUser $subwawUserPwd

# à refaire pour ajouter de nouveaux utilisateurs ou changer de mdp pour un user

# restart apache2 server
sudo docker exec -i -t  sub-local /etc/init.d/apache2 reload

L'interface subwaw est accessible à l'adresse 127.0.0.1/subwaw depuis la même machine.

Sinon exécuter cette commande pour avoir l'adresse IP public de la machine sur laquelle vous venez de déployer mbb-framework :
 
```bash
 wget -qO- https://ipinfo.io/ip | awk -v port=80 '{print "You can access the SubWaw workflow generator interface at :  http://"$1":"port}'
```


 pour avoir une console ssh à l'interieur du container :

```bash
sudo docker exec -i -t  sub-local /bin/bash
```




## Comment ajouter une brique outil (brique bleu) dans Subwaw ?


Il faut pour cela rajouter un outil dans le projet [WAW](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/tree/master/tools) puis mettre à jour sur la machine en production.


## Comment ajouter une brique data (brique verte) dans Subwaw ?

Il faut modifier le fichier [global.yaml](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/blob/master/tools/global.yaml) puis mettre à jour sur la machine en production.

## Comment ajouter un workflow dans le [manager](http://162.38.181.47/subwaw/workflowmanager.php) ?

Il faut ajouter un workflow généré par subwaw puis créer un nouveau workflow dans [WAW](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/tree/master/workflows). 

Il faut 3 fichiers :
1. le fichier snakefile
2. le fichier yaml
3. le fichier json (généré par subwaw)


## Comment modifier une brique outil (brique bleu) dans Subwaw ?

Il faut pour cela modifier le fichier yaml du projet correspondant dans le projet [WAW](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/tree/master/tools) puis mettre à jour sur la machine en production.

## Comment modifier une brique data (brique verte) dans Subwaw ?

Il faut modifier le fichier [global.yaml](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/blob/master/tools/global.yaml) puis mettre à jour sur la machine en production.


## Comment ajouter/modifier les catégories des briques dans Subwaw ?

Il faut modifier le fichier [global.yaml](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/blob/master/tools/global.yaml) puis mettre à jour sur la machine en production.

Si vous en ajoutez une nouvelle il faudra que dans l'outil que vous voulez mettre dans cette catégorie leur indiquer le nom de cette nouvelle catégorie.


## Où est le fichier de Configuration de Subwaw et comment le configurer sur une nouvelle machine ?

Le fichier de configuration n'est pas visible de base le dépôt Gitlab. Il faut copier le fichier conf/Conf.php.template pour créer le fichier conf/Conf.php afin de configurer le fichier sur une nouvelle machine de déploiement.
