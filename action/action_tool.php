<?php

define("PATH","..");
define("PAGE","actionContainer");

session_start ();

$action = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(!isset($_SESSION['username'])) {
    if($action != "download" && $action != "download_zip" && $action != "download_exist") {
        header("Location: ../login.php");
    }
}

require_once '../dao/DBquery.php';

$db = new DBqueryLite();

$today = date("Y-m-d G:i:s");

$ID            = 0;
$TID           = "";
$name          = "";
$description   = "";
$version       = "";
$website       = "";
$git           = "";
$documentation = "";
$article       = "";
$multiQC       = "";
$install       = "";
$yaml          = "";
$author        = "";
$creationDate  = "";
$updateDate    = "";
$labels        = array();
$tags          = "";



if(isset($_POST['toolid'])) {
    $id = $_POST['toolid'];
} else {
    if(isset($_GET['toolid'])) {
        $id = $_GET['toolid'];
    } else {
        $id = -1;
    }
}

if(isset($_POST['tid'])) {
    $tid = $_POST['tid'];
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['description'])) {
    $description = $_POST['description'];
}

if(isset($_POST['version'])) {
    $version = $_POST['version'];
}

if(isset($_POST['website'])) {
    $website = $_POST['website'];
}

if(isset($_POST['git'])) {
    $git = $_POST['git'];
}

if(isset($_POST['documentation'])) {
    $documentation = $_POST['documentation'];
}

if(isset($_POST['article'])) {
    $article = $_POST['article'];
}

if(isset($_POST['multiQC'])) {
    $multiQC = $_POST['multiQC'];
}

if(isset($_POST['install'])) {
    $install = $_POST['install'];
}

if(isset($_POST['yaml'])) {
    $yaml = $_POST['yaml'];
}

if(isset($_POST['creationDate'])) {
    $creationDate = $_POST['creationDate'];
}

$author = "MBB";

if($action == "create" || $action == "download") {
    $creationDate = date("Y-m-d");
}
else {
    $creationDate = $_POST['creationDate'];
}

$updateDate = date("Y-m-d");

if(isset($_POST['labels'])) {
    $labels = $_POST['labels'];
}

if(isset($_POST['tags'])) {
    $tags = str_replace("\r\n",' ', $_POST['tags']);
}

$tool = new Tool($id, $tid, $name, $description, $version, $website, $git, $documentation, $article, $multiQC, $install, $yaml, $author, $creationDate, $updateDate, $tags);

function izrand($length = 16) {
    $random_string = "";
    while(strlen($random_string)<$length && $length > 0) {
        $randnum = mt_rand(0,61);
        $random_string .= ($randnum < 10) ?
            chr($randnum+48) : ($randnum < 36 ? 
                chr($randnum+55) : chr($randnum+61));
    }
    return $random_string;
}

function rrmdir($dir) { 
    if (is_dir($dir)) { 
      $objects = scandir($dir);
      foreach ($objects as $object) { 
        if ($object != "." && $object != "..") { 
          if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
            rrmdir($dir. DIRECTORY_SEPARATOR .$object);
          else
            unlink($dir. DIRECTORY_SEPARATOR .$object); 
        } 
      }
      rmdir($dir); 
    } 
  }

  function zipData($source, $destination) {
    if (extension_loaded('zip')) {
        if (file_exists($source)) {
            $zip = new ZipArchive();
            if ($zip->open($destination, ZIPARCHIVE::CREATE)) {
                $source = realpath($source);
                if (is_dir($source)) {
                    $iterator = new RecursiveDirectoryIterator($source);
                    // skip dot files while iterating 
                    $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
                    $files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
                    foreach ($files as $file) {
                        $file = realpath($file);
                        if (is_dir($file)) {
                            $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                        } else if (is_file($file)) {
                            $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                        }
                    }
                } else if (is_file($source)) {
                    $zip->addFromString(basename($source), file_get_contents($source));
                }
            }
            return $zip->close();
        }
    }
    return false;
}


if($action == "create") {
    $tool->escape($db);
    $db->create($tool);

    $last_id = mysqli_insert_id($db->dbh);

    foreach ($labels as $lid){
        $contlab = new ToolLabel(0, $last_id, $lid);
        $db->create($contlab);
    }

    header("Location: ../toolmanager.php");
    
} else if ($action == "update") {
    $tool->escape($db);
    $db->update($tool);

    header("Location: ../toolmanager.php");
} else if ($action == "delete") {

    $tl = $db->getToolWithId($tool->ID);

    if($tl->author == $author) {
        $db->delete($tool);
    } else if($db->getGradeWithLogin($_SESSION['username'])->name == $db->getMaxGrades()->name) {
        $db->delete($tool);
    }

    header("Location: ../toolmanager.php");

} else if($action == "download") {

    $name = str_replace(' ', '_', $tool->TID);

    $randDir = izrand();

    $path_output = Conf::$PATH_YAML_OUTPUTS . $randDir."/";
    $path_output2 = $path_output . $name . "/";

    $path_yaml = $path_output2 . $name.".yaml";
    $path_snakefile = $path_output2 . $name.".rule.snakefile";

    mkdir($path_output, 0755, true);
    mkdir($path_output2, 0755, true);

    file_put_contents($path_yaml, $_POST['yaml']);

    $cmd = "PYTHONIOENCODING=utf-8 python3 " . Conf::$PATH_WAW . "generate_tool_snakefile.py " . $name . " " . $path_output;
    system($cmd);

    if(!class_exists('ZipArchive'))
        die("ZipArchive 3 NOT supported.");

    $filename = "/tmp/".rand(1,100000).".zip";

    zipData($path_output, $filename);

    rrmdir($path_output);
    

    if (file_exists($filename)) {
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="'.$name.'.zip"');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        unlink($filename);
    } 
    else {
        die("Zip not create");
    }
        
} else if($action == "download_zip") {

    //find "." -maxdepth 1 -mindepth 1 -type d -exec zip -r "{}.zip" "{}" \;

    $idtools = $_POST["idtools"];

    if(count($idtools) <= 0) {
        header("Location: ../toolmanager.php");
        return;
    } 

    if(!class_exists('ZipArchive'))
        die("ZipArchive 3 NOT supported.");

        $filename = "/tmp/".rand(1,100000).".zip";

        $zip = new ZipArchive();
        if ($zip->open($filename, ZipArchive::CREATE) === TRUE)
        {

            $base_path = Conf::$PATH_YAML;

            foreach ($idtools as $tid) {

                //$path = $base_path.$tid.".zip";
                $dir = $base_path.$tid."/";
                $zip->addEmptyDir($tid); 

                $dh = opendir($dir);
                while($file = readdir($dh))
                {
                    if ($file != '.' && $file != '..')
                    {
                        if (is_file($dir.$file))
                            $zip->addFile($dir.$file, $tid."/".$file);
                    }
                }
                closedir($dh); 
                

                //$zip->addFile($path, "tools/".$tid.".zip");

            }
            $zip->close();
        
        } else {
            echo 'Failed!';
            die();
        }

        if (file_exists($filename)) {
            header('Content-Type: application/zip');
            header('Content-Disposition: attachment; filename="tools.zip"');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            unlink($filename);
        } 
        else {
            die("Zip not create");
        }

} else if($action == "download_exist") {

    //$tl = $db->getToolWithId($tool->ID);

    /*$down = FALSE;
    if(($cnt->author == $author) || ($cnt->visibility == 1)) {
        $down = TRUE;
    } else if($db->getGradeWithLogin($_SESSION['username'])->name == $db->getMaxGrades()->name) {
        $down = TRUE;
    }
    */
    
    $down = TRUE;

    if($down) {

        $path_waw_tools = Conf::$PATH_YAML;

        $path_yaml = $path_waw_tools . $id . "/" . $id . ".yaml";
        
        if(file_exists($path_yaml)) {
        //$tool = $db->getToolWithId($idtool);

            $content = file_get_contents($path_yaml);
            $name = $id.".yml";

            $file = fopen($name,"wb");
            fwrite($file);
            fclose($file);

        }

        header('Content-Type: charset=utf-8');
        header("Content-disposition: attachment; filename=$name");

        print $content;
    } else {
        header("Location: ../toolmanager.php");
    }
} else {
    header("Location: ../toolmanager.php");
}


function createZip($zip,$dir){
    if (is_dir($dir)){
  
      if ($dh = opendir($dir)){
         while (($file = readdir($dh)) !== false){
   
           // If file
           if (is_file($dir.$file)) {
              if($file != '' && $file != '.' && $file != '..'){
   
                 $zip->addFile($dir.$file);
              }
           }else{
              // If directory
              if(is_dir($dir.$file) ){
  
                if($file != '' && $file != '.' && $file != '..'){
  
                  // Add empty directory
                  $zip->addEmptyDir($dir.$file);
  
                  $folder = $dir.$file.'/';
   
                  // Read data of the folder
                  createZip($zip,$folder);
                }
              }
   
           }
   
         }
         closedir($dh);
       }
    }
  }