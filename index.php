<?php

session_start ();

require_once "./inc/php/buildHeader.php";

?>
    <div class="container">
        <div class="row justify-content-center">
            <h1> SUBWAW </h1>
        </div>
        <div class="row justify-content-center">
            <h5>This application help to create tools and visually design workflows.</h5>
        </div>
        <br/>
        <br/>
        <div class="row">
        
            <div class="col-12">
                After downloading a zip file containing your workflow, you must unzip it and follow indications in the Readme.md file.
            </div>

            <div class="col-12" style="margin-top: 3em;">
                <h3>Here are some tips to enhance this web application :</h3>
   

                <div id="accordion" class="col-12">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Comment ajouter une brique outil (brique bleu) dans Subwaw ?
                            </button>
                        </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Il faut pour cela rajouter un outil dans le projet WAW puis mettre à jour sur la machine en production.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Comment ajouter une brique data (brique verte) dans Subwaw ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Il faut modifier le fichier global.yaml puis mettre à jour sur la machine en production.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Comment ajouter un workflow dans le manager ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">

                            <p class="col-12">Il faut ajouter un workflow généré par subwaw puis créer un nouveau workflow dans WAW.</p>

                            <p class="col-12">Il faut 3 fichiers :</p>

                            <ul>
                                <li>le fichier snakefile</li>
                                <li>le fichier yaml</li>
                                <li>le fichier json (généré par subwaw)

                            </ul>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading4">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                Comment modifier une brique outil (brique bleu) dans Subwaw ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                        <div class="card-body">
                        Il faut pour cela modifier le fichier yaml du projet correspondant dans le projet WAW puis mettre à jour sur la machine en production.
                        </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading5">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            Comment modifier une brique data (brique verte) dans Subwaw ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                        <div class="card-body">
                            Il faut modifier le fichier global.yaml puis mettre à jour sur la machine en production.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading6">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            Comment modifier une brique data (brique verte) dans Subwaw ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                        <div class="card-body">
                            Il faut modifier le fichier global.yaml puis mettre à jour sur la machine en production.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading7">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                            Comment ajouter/modifier les catégories des briques dans Subwaw ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                        <div class="card-body">
                            <p class="col-12">Il faut modifier le fichier global.yaml puis mettre à jour sur la machine en production.</p>
                            <p class="col-12">Si vous en ajoutez une nouvelle il faudra que dans l'outil que vous voulez mettre dans cette catégorie leur indiquer le nom de cette nouvelle catégorie.</p>

                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading8">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                            Où est le fichier de Configuration de Subwaw et comment le configurer sur une nouvelle machine ?
                            </button>
                        </h5>
                        </div>
                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                        <div class="card-body">
                            Le fichier de configuration n'est pas visible de base le dépôt Gitlab. Il faut copier le fichier conf/Conf.php.template pour créer le fichier conf/Conf.php afin de configurer le fichier sur une nouvelle machine de déploiement.
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>


</body>
</html>