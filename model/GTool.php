<?php


class GTool {

    var $name = "";
    var $text = "";
    var $version = "unknown";
    var $category = "Tool";
    var $categoryT = "";
    var $size2 = "";
    var $description = "unknown";
    var $figure = "Rectangle";
    var $fill = "#3498db";
    var $strokeWidth = 2;
    var $inservices = [];
    var $outservices = [];
    var $outservices2 = [];

    public function __construct($name, $text, $categoryT, $description, $version) {
        $this->name = $name;
        $this->text = $text;
        $this->categoryT = $categoryT;
        $this->description = ($description == "") ? "unknown" : $description;
        $this->version = ($version == "") ? "unknown" : $version;
    }

    public function jsonSerialize() {

        $array = array(
            "name" => $this->name,
            "text" => $this->text,
            "version" => $this->version,
            "category" => $this->category,
            "categoryT" => $this->categoryT,
            "size2" => $this->size2,
            "description" => $this->description,
            "figure" => $this->figure,
            "fill"=> $this->fill,
            "strokeWidth" => $this->strokeWidth,
            "inservices" => [],
            "outservices" => [],
            "outservices2" => []
        );

        foreach ($this->inservices as $port) {
            array_push($array["inservices"], $port->jsonSerialize()); 
        }

        foreach ($this->outservices as $port) {
            array_push($array["outservices"], $port->jsonSerialize()); 
        }

        foreach ($this->outservices2 as $port) {
            array_push($array["outservices2"], $port->jsonSerialize()); 
        }

        return json_encode($array, JSON_PRETTY_PRINT);
    }

    public function initSize() {
        $this->size2 = $this->getLSize($this->text)." ".$this->getRSize(max(sizeof($this->inservices), sizeof($this->outservices)));
    }

    public function initBlankSize() {
        $this->size2 = "1 55";
        $this->fill = "#FFFFFF";
        $this->strokeWidth = 0;
        $this->category = "blank";
    }

    public function generate() {

        $result = "{";

        $result .= 'name: "'.$this->name.'", '; 
        $result .= 'text: "'.$this->text.'", '; 
        
        $result .= 'size2: "'.$this->size2.'", '; 
        $result .= 'category: "'.$this->category.'", '; 
        $result .= 'strokeWidth: '.$this->strokeWidth.', '; 
        $result .= 'fill: "'.$this->fill.'", '; 
        $result .= 'inservices: [';

        $index = 0;
        foreach ($this->inservices as $port) {
            if($index == 0) {
                $result .= $port->generate();
            }
            else {
                $result .= ", ".$port->generate();
            }
            $index++;
        }

        $result .= '], ';

        $result .= 'outservices: [';

        $index = 0;
        foreach ($this->outservices as $port) {
            if($index == 0) {
                $result .= $port->generate();
            }
            else {
                $result .= ", ".$port->generate();
            }
            $index++;
        }
        
        $result .= '], ';

        $result .= 'outservices2: [';
        $result .= '{ name: "o'.(sizeof($this->outservices)+1).'", type: "link", value:"linkOut"}';
        $result .= '], ';

        $result .= 'description: "'.$this->description.'", ';
        $result .= 'version: "'.$this->version.'" ';

        $result .= "}";

        return $result;
    }

    public function addInputPort($gport) {
        array_push($this->inservices, $gport);
    }

    public function addOutputPort($gport) {
        array_push($this->outservices, $gport);
    }

    public function getLSize($value) {

        $size = strlen($value);

        $nspace = substr_count($value, ' ');

        if($size <= 10) {
            return 125 + $nspace * 5;
        }
        else if($size <= 15) {
            return 150 + $nspace * 5;
        }
        else if($size <= 20) {
            return 175 + $nspace * 10;
        }
        else if($size <= 25) {
            return 200 + $nspace * 10;
        }
        else {
            return 250 + $nspace * 15;
        }


    }
    
    public function getRSize($n) {


        return 35 + ($n*25);
    }


}