<?php

require_once(__DIR__.'/../dao/DBquery.php');

class User 
{
    var $ID      = 0;
    var $login   = "";
    var $gradeId = 0;

    /**
     * User constructor.
     * @param int $ID
     * @param string $name
     * @param string $color
     * @param int $gradeId
     */
    public function __construct($ID, $login, $gradeId)
    {
        $this->ID      = $ID;
        $this->login   = $login;
        $this->gradeId = $gradeId;
    }

    public function escape($db) {
    }

    public function getInsert() {
        return "INSERT INTO User (login, gradeId)
        VALUES ('$this->login', '$this->gradeId');";
    }

    public function getUpdate() {
        return "UPDATE User
        SET login='$this->login', gradeId='$this->gradeId'
        WHERE ID = '$this->ID';";

    }

    public function getDelete() {
        return "DELETE FROM User WHERE ID = '$this->ID';";
    }
}