<?php

require_once(__DIR__.'/../dao/DBquery.php');

class Label
{
    var $ID    = 0;
    var $name  = "";
    var $color = "#FFFFFF";
    var $gradeId = 0;

    /**
     * Label constructor.
     * @param int $ID
     * @param string $name
     * @param string $color
     * @param int $gradeId
     */
    public function __construct($ID, $name, $color, $gradeId)
    {
        $this->ID      = $ID;
        $this->name    = $name;
        $this->color   = $color;
        $this->gradeId = $gradeId;
    }

    public function escape($db) {
        $this->name  = utf8_decode($db->escape($this->name));
        $this->color = utf8_decode($db->escape($this->color));
    }

    public function getInsert() {
        return "INSERT INTO Label (name, color, gradeId)
        VALUES ('$this->name', '$this->color', '$this->gradeId');";
    }

    public function getUpdate() {
        return "UPDATE Label
        SET name='$this->name', color='$this->color', gradeId='$this->gradeId'
        WHERE ID = '$this->ID';";

    }

    public function getDelete() {
        return "DELETE FROM Label WHERE ID = '$this->ID';";
    }

}
