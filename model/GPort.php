<?php

class GPort {

    var $name = "";
    var $type = "";
    var $value = "";
    var $description = "";
    var $ptype = "OUT";
    var $from = "";
    var $list = false;


    public function __construct($name, $type, $value, $description, $ptype, $from = "", $list = false) {
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
        $this->description = $description;
        $this->ptype = $ptype;
        $this->from = $from;
        $this->list = $list;
    }

    public function jsonSerialize() {

        $array = array(
            "name" => $this->name,
            "type" => $this->type,
            "value" => $this->value,
            "description" => $this->description,
            "ptype" => $this->ptype,
            "from" => $this->from,
            "list" => $this->list
        );

        return json_encode($array, JSON_PRETTY_PRINT);
    }


    public function generate() {


        $type = "[";

        if(is_array($this->type)) {

            for ($i=0; $i < count($this->type); $i++) { 
                if($i > 0) {
                    $type .= ", ";
                }
                $type .= '"'.$this->type[$i].'"';
            }

            $type .= "]";
        }
        else {
            $type = '"'.$this->type.'"';
        }

        $stroke = "#000000";

        if($this->list) {
            $stroke = "#8e44ad";
        }
        
        $result = '{name: "'.$this->name.'", type: '.$type.', value: "'.$this->value.'", description: "'.$this->description.'", ptype: "'.$this->ptype.'", from: "'.$this->from.'", list: '.json_encode($this->list).', stroke: "'.$stroke.'"}';

        return $result;
    }

}



?>