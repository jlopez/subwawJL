<?php

require_once(__DIR__.'/../dao/DBquery.php');

class Grade
{
    var $ID    = 0;
    var $name  = "";
    var $level = 3;

    /**
     * Grade constructor.
     * @param int $ID
     * @param string $name
     * @param int $level
     */
    public function __construct($ID, $name, $level)
    {
        $this->ID    = $ID;
        $this->name  = $name;
        $this->level = $level;
    }

    public function escape($db) {
        $this->name  = utf8_decode($db->escape($this->name));
        $this->level = utf8_decode($db->escape($this->level));
    }

    public function getInsert() {
        return "INSERT INTO Grade (name, level)
        VALUES ('$this->name', '$this->level');";
    }

    public function getUpdate() {
        return "UPDATE Grade
        SET name='$this->name', level='$this->level'
        WHERE ID = '$this->ID';";

    }

    public function getDelete() {
        return "DELETE FROM Grade WHERE ID = '$this->ID';";
    }
}