<?php

require_once(__DIR__.'/../dao/DBquery.php');

class Workflow {

    var $ID           = 0;
    var $name         = "";
    var $docker_name  = "";
    var $description  = "";
    var $version      = "";
    var $author       = "";
    var $creationDate = "";
    var $updateDate   = "";
    var $tags         = "";
    var $graphicYAML  = "";
    var $yaml         = "";
    var $git          = "";

    public function __construct($ID, $name, $docker_name, $description, $version, $author, $creationDate, $updateDate, $tags, $graphicYAML, $yaml, $git)
    {
        $this->ID            = $ID;
        $this->name          = $name;
        $this->docker_name   = $docker_name;
        $this->description   = $description;
        $this->version       = $version;
        $this->author        = $author;
        $this->creationDate  = $creationDate;
        $this->updateDate    = $updateDate;
        $this->tags          = $tags;
        $this->graphicYAML   = $graphicYAML;
        $this->yaml          = $yaml;
        $this->git           = $git;
    }

    public function escape($db) {
        $this->name        = utf8_decode($db->escape($this->name));
        $this->docker_name = utf8_decode($db->escape($this->docker_name));
        $this->description = utf8_decode($db->escape($this->description));
        $this->version     = utf8_decode($db->escape($this->version));
        $this->tags        = utf8_decode($db->escape($this->tags));
        $this->graphicYAML = utf8_decode($db->escape($this->graphicYAML));
        $this->yaml        = utf8_decode($db->escape($this->yaml));
    }
    
    public function escape2($db) {
        $this->name        = utf8_encode($db->escape($this->name));
        $this->docker_name = utf8_encode($db->escape($this->docker_name));
        $this->description = utf8_encode($db->escape($this->description));
        $this->version     = utf8_encode($db->escape($this->version));
        $this->tags        = utf8_encode($db->escape($this->tags));
        $this->graphicYAML = utf8_encode($db->escape($this->graphicYAML));
        $this->yaml        = utf8_encode($db->escape($this->yaml));
    }

    public function getInsert() {
        return "INSERT INTO Workflow (name, docker_name, description, version, yaml, author, creationDate, updateDate, tags, graphicYAML, git)
        VALUES ('$this->name', '$this->docker_name', '$this->description', '$this->version', '$this->yaml', '$this->author', '$this->creationDate', '$this->updateDate', '$this->tags', '$this->graphicYAML', '$this->git');";
    }

    public function getUpdate() {
        return "UPDATE Workflow
        SET name ='$this->name', docker_name ='$this->docker_name', description ='$this->description', version ='$this->version', yaml ='$this->yaml', author ='$this->author', creationDate ='$this->creationDate', updateDate ='$this->updateDate', tags ='$this->tags', graphicYAML ='$this->graphicYAML', git ='$this->git'
        WHERE ID = '$this->ID';";
    }

    public function getDelete() {
        return "DELETE FROM Workflow WHERE ID = '$this->ID';";
    }
}