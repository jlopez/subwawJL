<?php

session_start ();

require_once "./inc/php/buildHeader.php";

require_once "./dao/DBquery.php";

require_once "./tool.php";

$db = new DBqueryLite();

$rtools = getYAMLTools();

?>

<div class="container-fluid">

<div class="row justify-content-md-center text-center">
    <div class="col-sm-2">
        <div class="card border-primary sm-2">
        <div class="card-header font-weight-bold text-white bg-primary"><h4>Tools</h4></div>
        <div class="card-body text-primary">
            <h3 class="card-title"><?php echo count($rtools); ?></h3>      
        </div>
        </div>
    </div>
</div>

<br/><br/>

<div class="row">
    <div class="col-12">
        <form action="./action/action_tool.php" method="post">

            <input type="text" class="form-control" style="display: none;" placeholder="action" name="action" value="download_zip">

            <button type="submit" class="btn btn-info"> <i class="fas fa-download"></i> Download selected tools</button>

            <a class="btn btn-success" href="./brick.php"> <i class="fas fa-plus-circle"></i> Create new tool</a>

            <br/><br/>

            <table id="Table_Container" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Choose</th>
                        <th>Name</th>
                        <th>Author</th>
                        <th>Label</th>
                        <th>Categories</th>
                        <th>Description</th>
                        <th>Version</th>
                        <th>Links</th>
                        <th>Article</th>
                        <!--<th>Creation</th>
                        <th>Update</th>-->
                        <th class="col-1"></th>
                    </tr>
                </thead>
                <tbody id="TableTool">

                <?php

                foreach ($rtools  as $tool) {
                 
                    $tool->escape2($db);
                    
                    echo '<tr>';
                    echo '<td><input type="checkbox" value="'.$tool->TID.'" name="idtools[]"></td>';
                    echo '<td>' . $tool->name . '</td>';
                    echo '<td>' . $tool->author . '</td>';

                    $labels = [];
                    array_push($labels, new Label(0, "MBB", "2ecc71", 1));
                    array_push($labels, new Label(0, "stable", "9b59b6", 2));

                    echo '<td>';

                    $index = 0;

                    foreach ($labels as $label) {
                        echo '<span class="badge" style="background:#'.$label->color.'">'.$label->name.'</span>';

                        if($index == 1) {
                            echo '<br/>';
                            $index = 0;
                        } else {
                            $index++;
                        }       
                    }
                    
                    echo '</td>';

                    $index = 0;

                    echo '<td>';
                    foreach (explode(",", $tool->tags) as $tag) {
                        echo '<span class="badge" style="background:#e67e22">'.str_replace("_", " ",$tag).'</span>';

                        if($index == 1) {
                            echo '<br/>';
                            $index = 0;
                        } else {
                            $index++;
                        }       
                    }
                    echo '</td>';
                    //echo '<td>' . $tool->tags . '</td>';

                    
                    echo '<td>' . $tool->description . '</td>';
                    echo '<td>' . $tool->version . '</td>';
 

                    echo '<td>';

                    if($tool->website != "") {
                        echo "<a href='".$tool->website."'>website</a></br>";
                    }
                    
                    if($tool->git != "") {
                        echo "<a href='".$tool->git."'>git</a></br>";
                    }
                    
                    if($tool->documentation != "") {
                        echo "<a href='".$tool->documentation."'>doc</a></br>";
                    }

                    echo '</div></td>';

                
                    echo '<td>' . $tool->article. '</td>';
                    

                    //echo '<td>' . $tool->creationDate . '</td>';
                    //echo '<td>' . $tool->updateDate . '</td>';
                    echo '<td>';

                    echo '<div class="btn-group" ><a class="btn btn-success btn-sm" onclick="showYamlTool(\''.$tool->TID.'\')"><i class="fas fa-eye"></i>&nbsp;YAML</a></div>';
                    echo '<div class="btn-group" >' . "<a class='btn btn-warning btn-sm' target='_blank' href='https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw/-/tree/master/tools/".$tool->TID."'><i class='fab fa-gitlab'></i>&nbsp;Gitlab</a></br>".'</div>';

                    echo '<div class="btn-group" ><a class="btn btn-info btn-sm" href="./action/action_tool.php?action=download_exist&toolid='.$tool->TID.'"><i class="fas fa-download"></i>&nbsp;Download</a></div>';
                    echo '<div class="btn-group"><a target="_blank" class="btn btn-primary btn-sm" href="./api/tools.php?tid='.$tool->TID.'" ><i class="fas fa-cogs"></i>&nbsp; API</a></div>';

                    if($tool->author == $_SESSION['username'] || $_SESSION['manage'] ) {
                        //echo '<div class="btn-group"><a class="btn btn-danger btn-sm" href="./action/action_tool.php?action=delete&toolid='.$tool->ID.'" >delete</a></div>';
                    }

                    //echo '<div class="btn-group"><a class="btn btn-warning btn-sm" href="./brick.php?toolid='.$tool->ID.'" >import</a></div>';

                    echo '</td>';

                    echo '</tr>';

                }


                ?>


                </tbody>
            </table>
        </form>
    </div>
</div>


    <!-- Start Modal Publish -->
    <div class="modal" id="modalShowTool">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header">
                    <h4 class="modal-title">Tool yaml</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <textarea type="text" class="form-control" id="valueToolFile" name="value" value=""></textarea>
                </div>
                <div class="modal-footer">
                    <!--<button type="submit" class="btn btn-success">Import</button>-->
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
    <!-- End Modal Publish -->

    

</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>
<script src = "./inc/js/workflow.js"></script>