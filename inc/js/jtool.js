if(SUBWAW === undefined) {
    var SUBWAW = {};
}

SUBWAW.JTool = function(data) {

    this.name = data.name;
    this.version = data.version;
    this.description = data.description;
    this.steps = [];
    this.INPorts = [];
    this.OUTPorts = [];

    this.addSteps = function() {
      for( var key in MBB_STEP_CATEGORY ) {
        var values = MBB_STEP_CATEGORY[key];

        if(values.includes(this.name)) {
          this.steps.push(key); 
        }
      }
    }

    this.addInputs = function(data) {
      if(data.inservices != undefined) {
        for (var i = 0; i < (data.inservices.length); i++) {
          var jport = new SUBWAW.JPort(data.inservices[i]);
          jport.initType(data.inservices[i]);
          this.INPorts.push(jport);
        }
      }
    }

    this.addOutputs = function(data) {
      if(data.outservices != undefined) {
        for (var i = 0; i < (data.outservices.length); i++) {
          var jport = new SUBWAW.JPort(data.outservices[i]);
          jport.initType(data.outservices[i]);
          this.OUTPorts.push(jport);
        }
      }
    }
  }