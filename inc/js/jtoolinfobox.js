if(SUBWAW === undefined) {
    var SUBWAW = {};
}

SUBWAW.JToolInfoBox = function(tool) {

    this.tool = tool;
    
    this.addVersionElement = function(infobox) {
      var element = document.createElement("div");
      element.textContent = "Version: " + this.tool.version;
      element.classList.add("infobox_child");
      infobox.appendChild(element);
    }

    this.addStepsElement = function(infobox) {
      var child = document.createElement("div");
      child.textContent = "Steps: ";
      child.classList.add("infobox_child");

      var ul = document.createElement("ul");
      for (var i = 0; i < (this.tool.steps.length); i++) {
        var step = this.tool.steps[i];
        var li = document.createElement("li");
        li.textContent = step;
        ul.appendChild(li);
      }

      child.appendChild(ul);
      infobox.appendChild(child);
    }

    this.addDescriptionElement = function(infobox) {
      if(this.tool.description != undefined) {
        var child = document.createElement("div");
        child.textContent = "Description: " + this.tool.description;
        infobox.appendChild(child);
      }
    }

    this.addPorts = function(infobox, type) {

      var text = (type == "IN") ? "Inputs: " : "Outputs: ";
      var ports = (type == "IN") ? this.tool.INPorts : this.tool.OUTPorts;


      if(ports.length > 0) {

        var child = document.createElement("div");
        child.textContent = text;
        child.classList.add("infobox_child");

        var ul = document.createElement("ul");

        for (var i = 0; i < (ports.length); i++) {
          var jport = ports[i];
          var li = document.createElement("li");
          var subul = document.createElement("ul");
          var subli2 = document.createElement("li");
          subli2.textContent = "desc: " + jport.description;
          subul.appendChild(subli2);
          li.textContent = jport.name + ": " + jport.type;

          if(jport.from != "") {
            li.textContent+= " (" + jport.from + ")";
          }

          if(jport.list) {
            li.textContent+= " (" + "list" + ")";
          }


          li.appendChild(subul);
          ul.appendChild(li);
        }

        child.appendChild(ul);
        infobox.appendChild(child);

      }
    }
  }