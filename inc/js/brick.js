function escapeHtml(text) {
    var map = {
      '"': '&quot;',
      "'": '&#039;'
    };
   
    return text.replace(/["']/g, function(m) { return map[m]; });
}

INPUTS_COUNT = 0;
OUTPUTS_COUNT = 0;
COMMAND_COUNT = 0;
RADIO_COUNT = 0;
SELECT_COUNT = 0;
OPTION_COUNT = 0;

function deleteInput(id) {
    $("#"+id).remove();
}

function addNumeric() {

    var prename = escapeHtml(document.getElementById("nameNumericOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixNumericOption").value) + "\",\n";
    var value = "\tvalue: " + escapeHtml(document.getElementById("valueNumericOption").value) + ",\n";
    var min = "\tmin: " + escapeHtml(document.getElementById("minNumericOption").value) + ",\n";
    var max = "\tmax: " + escapeHtml(document.getElementById("maxNumericOption").value) + ",\n";
    var step = "\tstep: " + escapeHtml(document.getElementById("stepNumericOption").value) + ",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelNumericOption").value) + "\",\n";
    var type = "\ttype: \"numeric\"";

    var yml = "\t{" + name + prefix + value + min + max + step + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+"numeric"+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameNumericOption").value = "";
    document.getElementById("prefixNumericOption").value = "";
    document.getElementById("valueNumericOption").value = "";
    document.getElementById("minNumericOption").value = "";
    document.getElementById("maxNumericOption").value = "";
    document.getElementById("stepNumericOption").value = "";
    document.getElementById("labelNumericOption").value = "";
}

function addNumericRange() {

    var prename = escapeHtml(document.getElementById("nameNumericRangeOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixNumericRangeOption").value) + "\",\n";
    var value = "\tvalue: \"" + escapeHtml(document.getElementById("value1NumericRangeOption").value) + "," + escapeHtml(document.getElementById("value1NumericRangeOption").value) + "\",\n";
    var min = "\tmin: " + escapeHtml(document.getElementById("minNumericRangeOption").value) + ",\n";
    var max = "\tmax: " + escapeHtml(document.getElementById("maxNumericRangeOption").value) + ",\n";
    var step = "\tstep: " + escapeHtml(document.getElementById("stepNumericRangeOption").value) + ",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelNumericRangeOption").value) + "\",\n";
    var type = "\ttype: \"numeric\"";

    var yml = "\t{" + name + prefix + value + min + max + step + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+"numericrange"+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );

    document.getElementById("nameNumericRangeOption").value = "";
    document.getElementById("prefixNumericRangeOption").value = "";
    document.getElementById("value1NumericRangeOption").value = "";
    document.getElementById("value2NumericRangeOption").value = "";
    document.getElementById("minNumericRangeOption").value = "";
    document.getElementById("maxNumericRangeOption").value = "";
    document.getElementById("stepNumericRangeOption").value = "";
    document.getElementById("labelNumericRangeOption").value = "";

}

function addFile() {
    var prename = escapeHtml(document.getElementById("nameFileOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixFileOption").value) + "\",\n";
    var value = "\tvalue: \"" + escapeHtml(document.getElementById("valueFileOption").value) + "\",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelFileOption").value) + "\",\n";
    var pretype = document.getElementById("typeFileOption").value;
    var type = "\ttype: \"" + pretype + "\"";

    var yml = "\t{" + name + prefix + value + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+pretype+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameFileOption").value = "";
    document.getElementById("prefixFileOption").value = "";
    document.getElementById("valueFileOption").value = "";
    document.getElementById("labelFileOption").value = "";
}

function addSelect2() {
    var prename = escapeHtml(document.getElementById("nameSelectOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixSelectOption").value) + "\",\n";
    var value = "\tvalue: \"" + escapeHtml(document.getElementById("valueSelectOption").value) + "\",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelSelectOption").value) + "\",\n";
    
    var choices = "\tchoices: [";
    

    var selectsTable = document.getElementById("selectsTable");
    for (var i = 0, row; row = selectsTable.rows[i]; i++) {
        var iname = escapeHtml(row.cells[1].childNodes[0].value);
        var ivalue = escapeHtml(row.cells[2].childNodes[0].value);
        var yaml = "";
        if(i > 0) {
            yaml += ", ";
        }
        yaml += iname +": \"" + ivalue + "\"";
        choices += yaml;
    }

    choices += "],\n";
    
    var pretype = "select";
    var type = "\ttype: \"" + pretype + "\"";

    var yml = "\t{" + name + prefix + value + choices + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+pretype+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameSelectOption").value = "";
    document.getElementById("prefixSelectOption").value = "";
    document.getElementById("valueSelectOption").value = "";
    document.getElementById("labelSelectOption").value = "";
    document.getElementById("selectsTable").innerHTML = "";
}

function addRadio2() {
    var prename = escapeHtml(document.getElementById("nameRadioOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixRadioOption").value) + "\",\n";
    var value = "\tvalue: \"" + escapeHtml(document.getElementById("valueRadioOption").value) + "\",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelRadioOption").value) + "\",\n";
    
    var choices = "\tchoices: [";
    

    var radiosTable = document.getElementById("radiosTable");
    for (var i = 0, row; row = radiosTable.rows[i]; i++) {
        var iname = escapeHtml(row.cells[1].childNodes[0].value);
        var ivalue = escapeHtml(row.cells[2].childNodes[0].value);
        var yaml = "";
        if(i > 0) {
            yaml += ", ";
        }
        yaml += iname +": \"" + ivalue + "\"";
        choices += yaml;
    }

    choices += "],\n";
    
    var pretype = "radio";
    var type = "\ttype: \"" + pretype + "\"";

    var yml = "\t{" + name + prefix + value + choices + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+pretype+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameRadioOption").value = "";
    document.getElementById("prefixRadioOption").value = "";
    document.getElementById("valueRadioOption").value = "";
    document.getElementById("labelRadioOption").value = "";
    document.getElementById("radiosTable").innerHTML = "";
}

function addCheckbox() {
    var prename = escapeHtml(document.getElementById("nameCheckboxOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixCheckboxOption").value) + "\",\n";
    var value = "";
    if(document.getElementById("value1CheckboxOption").checked) {
        value = "\tvalue: " + document.getElementById("value1CheckboxOption").value + ",\n";
    }
    else {
        value = "\tvalue: " + document.getElementById("value2CheckboxOption").value + ",\n";
    }

    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelCheckboxOption").value) + "\",\n";
    var type = "\ttype: \"" + "checkbox" + "\"";

    var yml = "\t{" + name + prefix + value + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+"checkbox"+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameCheckboxOption").value = "";
    document.getElementById("prefixCheckboxOption").value = "";
    document.getElementById("labelCheckboxOption").value = "";
}

function addDirectory() {
    var prename = escapeHtml(document.getElementById("nameDirectoryOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixDirectoryOption").value) + "\",\n";
    var value = "\tvalue: \"" + escapeHtml(document.getElementById("valueDirectoryOption").value) + "\",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelDirectoryOption").value) + "\",\n";
    var pretype = document.getElementById("typeDirectoryOption").value;
    var type = "\ttype: \"" + pretype + "\"";

    var yml = "\t{" + name + prefix + value + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+pretype+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameDirectoryOption").value = "";
    document.getElementById("prefixDirectoryOption").value = "";
    document.getElementById("valueDirectoryOption").value = "";
    document.getElementById("labelDirectoryOption").value = "";
}

function addText() {

    var prename = escapeHtml(document.getElementById("nameTextOption").value);
    var name = "name: \"" + prename + "\",\n";
    var prefix = "\tprefix: \"" + escapeHtml(document.getElementById("prefixTextOption").value) + "\",\n";
    var value = "\tvalue: \"" + escapeHtml(document.getElementById("valueTextOption").value) + "\",\n";
    var label = "\tlabel: \"" + escapeHtml(document.getElementById("labelTextOption").value) + "\",\n";
    var type = "\ttype: \"text\"";

    var yml = "\t{" + name + prefix + value + label + type + "\n\t}";

    var resultTable = `
    <tr id="option_tr_`+OPTION_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('option_tr_`+OPTION_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOptionCommand_`+OPTION_COUNT+`" value="`+prename+`"></td>
                <td><input type="text" class="form-control" id="typeOptionCommand_`+OPTION_COUNT+`" value="`+"text"+`"></td>
                <td><textarea type="text" class="form-control" rows="7" id="valueOptionCommand_`+OPTION_COUNT+`" >`+yml+`</textarea></td> 
            </tr>
    `;

    OPTION_COUNT++;

    $( "#optionsTable").append( resultTable );


    document.getElementById("nameTextOption").value = "";
    document.getElementById("prefixTextOption").value = "";
    document.getElementById("valueTextOption").value = "";
    document.getElementById("labelTextOption").value = "";
}

function addRadio() {
    var name = document.getElementById("nameRadioCommand").value;
    var value = document.getElementById("valueRadioCommand").value;

    var resultTable = `
    <tr id="radio_tr_`+RADIO_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('radio_tr_`+RADIO_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameRadioCommand_`+RADIO_COUNT+`" value="`+name+`"></td>
                <td><input type="text" class="form-control" id="valueRadioCommand_`+RADIO_COUNT+`" value="`+value+`"></td> 
            </tr>
    `;

    RADIO_COUNT++;

    $( "#radiosTable").append( resultTable );
}

function addSelect() {
    var name = document.getElementById("nameSelectCommand").value;
    var value = document.getElementById("valueSelectCommand").value;

    var resultTable = `
    <tr id="select_tr_`+SELECT_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('select_tr_`+SELECT_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameSelectCommand_`+SELECT_COUNT+`" value="`+name+`"></td>
                <td><input type="text" class="form-control" id="valueSelectCommand_`+SELECT_COUNT+`" value="`+value+`"></td> 
            </tr>
    `;

    SELECT_COUNT++;

    $( "#selectsTable").append( resultTable );
}

function addInput() {

    var name = document.getElementById("nameInputCommand").value;
    var file = document.getElementById("fileInputCommand").value;
    var description = document.getElementById("descriptionInputCommand").value;

    var resultTable = `
    <tr id="input_tr_`+INPUTS_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('input_tr_`+INPUTS_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameInputCommand_`+INPUTS_COUNT+`" value="`+name+`"></td>
                <td><input type="text" class="form-control" id="fileInputCommand_`+INPUTS_COUNT+`" value="`+file+`"></td> 
                <td><input type="text" class="form-control" id="descriptionInputCommand_`+INPUTS_COUNT+`" value="`+description+`"></td>
            </tr>
    `;

    INPUTS_COUNT++;

    $( "#inputsTable").append( resultTable );
}

function addOutput() {

    var name = document.getElementById("nameOutputCommand").value;
    var file = document.getElementById("fileOutputCommand").value;
    var description = document.getElementById("descriptionOutputCommand").value;

    var resultTable = `
    <tr id="output_tr_`+OUTPUTS_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('output_tr_`+OUTPUTS_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameOutputCommand_`+OUTPUTS_COUNT+`" value="`+name+`"></td>
                <td><input type="text" class="form-control" id="fileOutputCommand_`+OUTPUTS_COUNT+`" value="`+file+`"></td> 
                <td><input type="text" class="form-control" id="descriptionOutputCommand_`+OUTPUTS_COUNT+`" value="`+description+`"></td>
            </tr>
    `;

    OUTPUTS_COUNT++;

    $( "#outputsTable").append( resultTable );

}

function showOptionModal() {
    var select = document.getElementById("optionTypeSelect").value;
    $( "#"+select+"Modal").modal( "show" );
}

function addCommand() {

    var name = escapeHtml(document.getElementById("nameCommand").value);

    var text = escapeHtml(document.getElementById("textCommand").value);

    var category = escapeHtml(document.getElementById("categoryCommand").value);

    var command = escapeHtml(document.getElementById("commandCommand").value);

    var inputs = "";
    var inputsTable = document.getElementById("inputsTable");
    for (var i = 0, row; row = inputsTable.rows[i]; i++) {
        var iname = escapeHtml(row.cells[1].childNodes[0].value);
        var ifile = escapeHtml(row.cells[2].childNodes[0].value);
        var idescription = escapeHtml(row.cells[3].childNodes[0].value);
        var yaml = "";
        if(i > 0) {
            yaml += ",\n";
        }
        yaml += "\t{name: \""+iname+"\", file: \""+ifile+"\", description: \""+idescription+"\"}";
        inputs += yaml;
    }


    var outputs = "";
    var outputsTable = document.getElementById("outputsTable");
    for (var i = 0, row; row = outputsTable.rows[i]; i++) {
        var iname = escapeHtml(row.cells[1].childNodes[0].value);
        var ifile = escapeHtml(row.cells[2].childNodes[0].value);
        var idescription = escapeHtml(row.cells[3].childNodes[0].value);
        var yaml = "";
        if(i > 0) {
            yaml += ",\n";
        }
        yaml += "\t{name: \""+iname+"\", file: \""+ifile+"\", description: \""+idescription+"\"}";
        outputs += yaml;
    }

    var output_dir = escapeHtml(document.getElementById("outputDirCommand").value);

    var options = "";
    var optionsTable = document.getElementById("optionsTable");
    for (var i = 0, row; row = optionsTable.rows[i]; i++) {
        var ivalue = row.cells[3].childNodes[0].value;
        var yaml = "";
        if(i > 0) {
            yaml += ",\n";
        }
        yaml += ivalue;
        options += yaml;
    }

    var resultTable = `
    <tr id="command_tr_`+COMMAND_COUNT+`">
                <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteInput('command_tr_`+COMMAND_COUNT+`')"> X </button></td>
                <td><input type="text" class="form-control" id="nameCommandCommand_`+COMMAND_COUNT+`" value="`+name+`"></td>
                <td><input type="text" class="form-control" id="textCommandCommand_`+COMMAND_COUNT+`" value="`+text+`"></td>
                <td><input type="text" class="form-control" id="categoryCommandCommand_`+COMMAND_COUNT+`" value="`+category+`"></td>
                <td><textarea type="text" class="form-control" rows="5" id="commandCommandCommand_`+COMMAND_COUNT+`" >`+command+`</textarea></td> 
                <td><textarea type="text" class="form-control" rows="5" id="inputsCommandCommand_`+COMMAND_COUNT+`">`+inputs+`</textarea></td>
                <td><textarea type="text" class="form-control" rows="5" id="outputsCommandCommand_`+COMMAND_COUNT+`">`+outputs+`</textarea></td>
                <td><input type="text" class="form-control" id="outputDirCommandCommand_`+COMMAND_COUNT+`" value="`+output_dir+`"></td>
                <td><textarea type="text" class="form-control" rows="5" id="optionsCommandCommand_`+COMMAND_COUNT+`">`+options+`</textarea></td>
            </tr>
    `;

    COMMAND_COUNT++;

    $( "#commandsTable").append( resultTable );

    document.getElementById("nameCommand").value = "";
    document.getElementById("textCommand").value = "";
    document.getElementById("categoryCommand").value = "";
    document.getElementById("commandCommand").value = "";
    document.getElementById("inputsTable").innerHTML = "";
    document.getElementById("outputsTable").innerHTML = "";
    document.getElementById("outputDirCommand").value = "";
    document.getElementById("optionsTable").innerHTML = "";
}

function downloadBrick() {

    var value = generateBrick();
    if(value != -1) {
        document.getElementById("yamlTool").value = document.getElementById("brickPreview").value.replace(/\t/g, '');;
        document.getElementById("downloadTool").submit();
    }
    //var nameTool= document.getElementById("NameBrick").value;
    //var result = document.getElementById("brickPreview").value;

    //var filename = nameTool+".yaml";

    //var element = document.createElement('a');
    //element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result));
    //element.setAttribute('download', filename);
    //element.style.display = 'none';
    //document.body.appendChild(element);

    //element.click();

    //document.body.removeChild(element);
    
}

var LABELID = [];


function showFormTool(action, tags) {

    var contentYAML = document.getElementById("brickPreview").value;

    if(contentYAML != "") {

        if(action == "create") {
            document.getElementById("titleTool").innerHTML = "Publish tool";

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            
            var yyyy = today.getFullYear();
            if (dd < 10) {
              dd = '0' + dd;
            } 
            if (mm < 10) {
              mm = '0' + mm;
            } 

            var date = yyyy + "-" + mm + "-" + dd;

            document.getElementById("creationDateTool").value = date;

            document.getElementById("tagsTool").value = tags;

            var elements = document.getElementById("labelsTool").options;

            LABELID = [];

            for(var i = 0; i < elements.length; i++){

            if(elements[i].selected) {
              LABELID.push(elements[i].value);
            }


              
              elements[i].selected = false;
            }

        } 
        else if(action == "update") {
            document.getElementById("titleTool").innerHTML = "Update tool";
            document.getElementById("creationDateTool").value = document.getElementById("CreationDate").value;

            document.getElementById("tagsTool").value = tags;

            var elements = document.getElementById("labelsTool").options;

            for(var i = 0; i < elements.length; i++){

                if(LABELID.includes(elements[i].value)) {
                    elements[i].selected = true;
                }
            
            }
        }

        document.getElementById("actionTool").value = action;
        document.getElementById("tidTool").value = document.getElementById("IDBrick").value;
        document.getElementById("nameTool").value = document.getElementById("NameBrick").value;
        document.getElementById("descriptionTool").value = document.getElementById("DescriptionBrick").value;
        document.getElementById("versionTool").value = document.getElementById("VersionBrick").value;
        document.getElementById("websiteTool").value = document.getElementById("WebsiteBrick").value;
        document.getElementById("gitTool").value = document.getElementById("GitBrick").value;
        document.getElementById("documentationTool").value = document.getElementById("DocumentationBrick").value;
        document.getElementById("articleTool").value = document.getElementById("ArticleBrick").value;
        document.getElementById("multiQCTool").value = document.getElementById("MultiQCBrick").value;
        document.getElementById("installTool").value = document.getElementById("InstallBrick").value;
        document.getElementById("valueTool").value = contentYAML;


        $('#modalSaveTool').modal();

    } 
    else {
        alert("Generate once before publish file.");
    }



}


function generateBrick() {

    var BYAML = "";
    var BROW = 0;

    BYAML += "{\n";
    BROW+=2;

    ALERT = "";

    var BID = document.getElementById("IDBrick").value.replace("/ /g","_");
    if(BID == "") {
        ALERT += "ID field is required.";
    }
    BYAML += "id: \"" + escapeHtml(BID) + "\",\n";
    BROW+=1;

    var BNAME = document.getElementById("NameBrick").value;
    if(BNAME == "") {
        ALERT += "Name field is required.";
    }
    BYAML += "name: \"" + escapeHtml(BNAME) + "\",\n";
    BROW+=1;

    var BDescription = document.getElementById("DescriptionBrick").value;
    if(BDescription == "") {
        ALERT += "Description field is required.";
    }
    BYAML += "description: \"" + escapeHtml(BDescription) + "\",\n";
    BROW+=1;

    var BVersion = document.getElementById("VersionBrick").value;
    if(BVersion == "") {
        ALERT += "Version field is required.";
    }
    BYAML += "version: \"" + escapeHtml(BVersion) + "\",\n";
    BROW+=1;

    var BWebsite = document.getElementById("WebsiteBrick").value;
    BYAML += "website: \"" + escapeHtml(BWebsite) + "\",\n";
    BROW+=1;

    var BGit = document.getElementById("GitBrick").value;
    BYAML += "git: \"" + escapeHtml(BGit) + "\",\n";
    BROW+=1;

    var BDocumentation = document.getElementById("DocumentationBrick").value;
    BYAML += "documentation: \"" + escapeHtml(BDocumentation) + "\",\n";
    BROW+=1;

    var BArticle = document.getElementById("ArticleBrick").value;
    BYAML += "article: \"" + escapeHtml(BArticle) + "\",\n";
    BROW+=1;

    var BMultiQC = document.getElementById("MultiQCBrick").value;
    if(BMultiQC == "") {
        ALERT += "MultiQC field is required. Default value is custom.";
    }
    BYAML += "multiqc: \"" + escapeHtml(BMultiQC) + "\",\n";
    BROW+=1;

    BYAML += "commands: [\n";
    BROW+=1;


    var commands = "";
    var commandsTable = document.getElementById("commandsTable");
    for (var i = 0, row; row = commandsTable.rows[i]; i++) {
        var iname = row.cells[1].childNodes[0].value;
        var itext = row.cells[2].childNodes[0].value;
        var icategory = row.cells[3].childNodes[0].value;
        var icommand = row.cells[4].childNodes[0].value;
        var cmds = icommand.split("\n");

        icommand = "";

        cmds.forEach(function(element) {
            icommand += "\"" + escapeHtml(element) + "\",\n";
        });

        icommand = icommand.substring(0, icommand.length - 2);

        var iinputs = row.cells[5].childNodes[0].value;
        var ioutputs = row.cells[6].childNodes[0].value;
        var ioutput_dir = row.cells[7].childNodes[0].value;
        var ioptions = row.cells[8].childNodes[0].value;

        var yaml = "";
        if(i > 0) {
            yaml += ",\n";
        }

        yaml += "\n{name: \""+iname+"\",\ncname: \""+itext+"\",\ncategory: \""+icategory+"\",\n command: [\n"+icommand+"\n]"+
            ",\n inputs: [\n"+iinputs+"\n],\n outputs: [\n"+ioutputs+"\n],\n output_dir: \""+ioutput_dir+"\",\n options: [\n"+ioptions+"\n]\n}";

        commands += yaml;
        BROW+=1;
    }

    BYAML += commands;


    BYAML += "],\n"
    BROW+=2;


    BYAML += "install: {\n";

    var BInstall = document.getElementById("InstallBrick").value;

    var AInstall = BInstall.split("\n");

    BYAML += "\t" + BID + ": {\n";

    AInstall.forEach(function(element) {
        BYAML += "\t\t\"" + escapeHtml(element) + "\",\n";
        BROW+=1;
    });
    
    BYAML = BYAML.substring(0, BYAML.length - 2);
    BYAML += "\n\n\t}\n";
    BROW+=1;

    BYAML += "}\n"
    BROW+=2;

    BYAML += "}\n";
    BROW+=2;
    
    if(ALERT != "") {
        alert(ALERT);
        return -1;
    }
    else {
        $("#brickPreview").attr('rows', BROW);
        document.getElementById("brickPreview").value = BYAML; 
    }

    return BYAML;
}