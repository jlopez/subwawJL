<?php


require_once "../dao/DBquery.php";
require_once "../tool.php";

$tid = "";

if(isset($_GET['tid'])) {
    $tid = $_GET['tid'];
}

$tools = getJSONTools($tid);

header('Content-Type: application/json');

$json_pretty = json_encode($tools, JSON_PRETTY_PRINT);
echo $json_pretty;

?>